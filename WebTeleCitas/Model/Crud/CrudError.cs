﻿using Microsoft.EntityFrameworkCore;
using Model.Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.Crud
{
    public interface ICrudError<T> where T : Error
    {
        IQueryable<T> GetAll();
        T Get(int id);
        void Insert(T entity);
        void InsertWithTransaction(T entity);
        void Update(T entity);
        void Commit();
    }

    public class CrudError : ICrudError<Error>
    {
        private readonly CitasDbContext context;
        private readonly DbSet<Error> entities;

        public CrudError(CitasDbContext context)
        {
            this.context = context;
            entities = context.Set<Error>();
        }

        public void Commit()
        {
            try
            {
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Error Get(int id)
        {
            return entities.SingleOrDefault(s => s.ID == id);
        }

        public IQueryable<Error> GetAll()
        {
            return entities;
        }

        public void Insert(Error entity)
        {
            try
            {
                if (entity == null)
                {
                    throw new ArgumentNullException("entity");
                }
                entities.Add(entity);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertWithTransaction(Error entity)
        {
            try
            {
                if (entity == null)
                {
                    throw new ArgumentNullException("entity");
                }
                entities.Add(entity);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(Error entity)
        {
            try
            {
                if (entity == null)
                {
                    throw new ArgumentNullException("entity");
                }
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
