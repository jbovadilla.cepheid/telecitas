﻿using Microsoft.EntityFrameworkCore;
using Model.Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.Crud
{
    public interface ICrudParametro<T> where T : Parametro
    {
        IQueryable<T> GetAll();
        T Get(int id);
        void Insert(T entity);
        void InsertWithTransaction(T entity);
        void Update(T entity);
        void Commit();
    }

    public class CrudParametro : ICrudParametro<Parametro>
    {
        private readonly CitasDbContext context;
        private readonly DbSet<Parametro> entities;

        public CrudParametro(CitasDbContext context)
        {
            this.context = context;
            entities = context.Set<Parametro>();
        }

        public void Commit()
        {
            try
            {
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Parametro Get(int id)
        {
            return entities.SingleOrDefault(s => s.IPRMTRO == id);
        }

        public IQueryable<Parametro> GetAll()
        {
            return entities;
        }

        public void Insert(Parametro entity)
        {
            try
            {
                if (entity == null)
                {
                    throw new ArgumentNullException("entity");
                }
                entities.Add(entity);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertWithTransaction(Parametro entity)
        {
            try
            {
                if (entity == null)
                {
                    throw new ArgumentNullException("entity");
                }
                entities.Add(entity);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(Parametro entity)
        {
            try
            {
                if (entity == null)
                {
                    throw new ArgumentNullException("entity");
                }
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
