﻿using Microsoft.EntityFrameworkCore;
using Model.Entidad;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Model.Crud
{
    public interface ICrudConsultorio<T> where T : Consultorio
    {
        IQueryable<T> GetAll();
        T Get(int id);
        void Insert(T entity);
        void InsertWithTransaction(T entity);
        void Update(T entity);
        void Commit();

        void CreatewithProcedure(Consultorio model);
    }

    public class CrudConsultorio : ICrudConsultorio<Consultorio>
    {
        private readonly CitasDbContext context;
        private readonly DbSet<Consultorio> entities;
        
        public CrudConsultorio(CitasDbContext context)
        {
            this.context = context;
            entities = context.Set<Consultorio>();
        }

        public void Commit()
        {
            try
            {
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Consultorio Get(int id)
        {
            return entities.SingleOrDefault(s => s.Id == id);
        }

        public IQueryable<Consultorio> GetAll()
        {
            return entities;
        }

        public void Insert(Consultorio entity)
        {
            try
            {
                if (entity == null)
                {
                    throw new ArgumentNullException("entity");
                }
                entities.Add(entity);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertWithTransaction(Consultorio entity)
        {
            try
            {
                if (entity == null)
                {
                    throw new ArgumentNullException("entity");
                }
                entities.Add(entity);
                //context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(Consultorio entity)
        {
            try
            {
                if (entity == null)
                {
                    throw new ArgumentNullException("entity");
                }
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void CreatewithProcedure(Consultorio model)
        {
            var piso = new SqlParameter("@npiso", model.Piso);
            var descrip = new SqlParameter("@cdecrip", model.DConsultorio);
            context.Database.ExecuteSqlCommand("EXEC cit.AGREGARCONSULTORIO @npiso,@cdecrip", piso, descrip);
        }
    }
}
