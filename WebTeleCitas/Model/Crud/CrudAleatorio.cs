﻿using Microsoft.EntityFrameworkCore;
using Model.Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.Crud
{
    public interface ICrudAleatorio<T> where T : Aleatorio
    {
        IQueryable<T> GetAll();
        T Get(int id);
        void Insert(T entity);
        void InsertWithTransaction(T entity);
        void Update(T entity);
        void Commit();
    }

    public class CrudAleatorio : ICrudAleatorio<Aleatorio>
    {
        private readonly CitasDbContext context;
        private readonly DbSet<Aleatorio> entities;

        public CrudAleatorio(CitasDbContext context)
        {
            this.context = context;
            entities = context.Set<Aleatorio>();
        }

        public void Commit()
        {
            try
            {
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Aleatorio Get(int id)
        {
            return entities.SingleOrDefault(s => s.CRRLTVO == id);
        }

        public IQueryable<Aleatorio> GetAll()
        {
            return entities;
        }

        public void Insert(Aleatorio entity)
        {
            try
            {
                if (entity == null)
                {
                    throw new ArgumentNullException("entity");
                }
                entities.Add(entity);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertWithTransaction(Aleatorio entity)
        {
            try
            {
                if (entity == null)
                {
                    throw new ArgumentNullException("entity");
                }
                entities.Add(entity);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(Aleatorio entity)
        {
            try
            {
                if (entity == null)
                {
                    throw new ArgumentNullException("entity");
                }
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
