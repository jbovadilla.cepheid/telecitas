﻿using Microsoft.EntityFrameworkCore;
using Model.Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.Crud
{
    public interface ICrudPersona<T> where T : Persona
    {
        IQueryable<T> GetAll();
        T Get(int id);
        void Insert(T entity);
        void InsertWithTransaction(T entity);
        void Update(T entity);
        void Commit();

        T Login(string tipo, string numero, string clave);

        T GetXCorreo(string correo);
        T GetXTipNumero(string tipo, string numero);
    }

    public class CrudPersona : ICrudPersona<Persona>
    {
        private readonly CitasDbContext context;
        private readonly DbSet<Persona> entities;

        public CrudPersona(CitasDbContext context)
        {
            this.context = context;
            entities = context.Set<Persona>();
        }

        public void Commit()
        {
            try
            {
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Persona Get(int id)
        {
            return entities.SingleOrDefault(s => s.ID == id);
        }

        public IQueryable<Persona> GetAll()
        {
            return entities;
        }

        public Persona GetXCorreo(string correo)
        {
            return entities.SingleOrDefault(s => s.CRRO == correo);
        }

        public Persona GetXTipNumero(string tipo, string numero)
        {
            return entities.SingleOrDefault(s => s.TDCMNTO == tipo && s.NDCMNTO == numero);
        }

        public void Insert(Persona entity)
        {
            try
            {
                if (entity == null)
                {
                    throw new ArgumentNullException("entity");
                }
                entities.Add(entity);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public void InsertWithTransaction(Persona entity)
        {
            try
            {
                if (entity == null)
                {
                    throw new ArgumentNullException("entity");
                }
                entities.Add(entity);
                //context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Persona Login(string tipo, string numero, string clave)
        {
            return entities.SingleOrDefault(s => s.TDCMNTO == tipo && s.NDCMNTO == numero && s.CNTRSNA == clave); 
        }

        public void Update(Persona entity)
        {
            try
            {
                if (entity == null)
                {
                    throw new ArgumentNullException("entity");
                }
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}
