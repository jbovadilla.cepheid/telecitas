﻿using Microsoft.EntityFrameworkCore;
using Model.Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.Crud
{
    public interface ICrudEspecialidad<T> where T : Especialidad
    {
        IQueryable<T> GetAll();
        T Get(int id);
        void Insert(T entity);
        void InsertWithTransaction(T entity);
        void Update(T entity);
        void Commit();
    }

    public class CrudEspecialidad : ICrudEspecialidad<Especialidad>
    {
        private readonly CitasDbContext context;
        private readonly DbSet<Especialidad> entities;

        public CrudEspecialidad(CitasDbContext context)
        {
            this.context = context;
            entities = context.Set<Especialidad>();
        }

        public void Commit()
        {
            try
            {
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Especialidad Get(int id)
        {
            return entities.SingleOrDefault(s => s.ID == id);
        }

        public IQueryable<Especialidad> GetAll()
        {
            return entities;
        }

        public void Insert(Especialidad entity)
        {
            try
            {
                if (entity == null)
                {
                    throw new ArgumentNullException("entity");
                }
                entities.Add(entity);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertWithTransaction(Especialidad entity)
        {
            try
            {
                if (entity == null)
                {
                    throw new ArgumentNullException("entity");
                }
                entities.Add(entity);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(Especialidad entity)
        {
            try
            {
                if (entity == null)
                {
                    throw new ArgumentNullException("entity");
                }
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
