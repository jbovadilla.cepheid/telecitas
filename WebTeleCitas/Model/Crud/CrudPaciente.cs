﻿using Microsoft.EntityFrameworkCore;
using Model.Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model.Crud
{
    public interface ICrudPaciente<T> where T : Paciente
    {
        IQueryable<T> GetAll();
        T Get(int id);
        void Insert(T entity);
        void InsertWithTransaction(T entity);
        void Update(T entity);
        void Commit();

        T GetXIdPersona(int idpersona);
        IQueryable<T> GetXIdTitular(int idtitular);
    }

    public class CrudPaciente : ICrudPaciente<Paciente>
    {
        private readonly CitasDbContext context;
        private readonly DbSet<Paciente> entities;

        public CrudPaciente(CitasDbContext context)
        {
            this.context = context;
            entities = context.Set<Paciente>();
        }

        public void Commit()
        {
            try
            {
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Paciente Get(int id)
        {
            return entities.SingleOrDefault(s => s.ID == id);
        }

        public IQueryable<Paciente> GetAll()
        {
            return entities;
        }

        public Paciente GetXIdPersona(int idpersona)
        {
            return entities.SingleOrDefault(s => s.IDSYS00 == idpersona);
        }

        public IQueryable<Paciente> GetXIdTitular(int idtitular)
        {
            return entities.Where(s => s.IDTTLR == idtitular && s.CESTDO == Codigos.ESTADO_A);
        }

        public void Insert(Paciente entity)
        {
            try
            {
                if (entity == null)
                {
                    throw new ArgumentNullException("entity");
                }
                entities.Add(entity);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertWithTransaction(Paciente entity)
        {
            try
            {
                if (entity == null)
                {
                    throw new ArgumentNullException("entity");
                }
                entities.Add(entity);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(Paciente entity)
        {
            try
            {
                if (entity == null)
                {
                    throw new ArgumentNullException("entity");
                }
                
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
