﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Model.Entidad;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Mapas
{
    public class Map_Aleatorio
    {
        public Map_Aleatorio(EntityTypeBuilder<Aleatorio> entity)
        {
            entity.HasKey(e => e.CRRLTVO);

            entity.ToTable("GCSYS10", "CIT");

            entity.Property(e => e.CRRLTVO)
                .HasColumnName("CRRLTVO")
                .ValueGeneratedOnAdd();

            entity.Property(e => e.CPRSNA)
                .IsRequired()
                .HasColumnName("CPRSNA")
                .IsUnicode(false);

            entity.Property(e => e.ALTRO)
                .IsRequired()
                .HasColumnName("ALTRO")
                .IsUnicode(false);
            

            entity.Property(e => e.CESTDO)
                .IsRequired()
                .HasColumnName("CESTDO")
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("('A')");

            entity.Property(e => e.FESTDO)
               .HasColumnName("FESTDO")
               .HasColumnType("datetime")
               .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.FCRCN)
                .HasColumnName("FCRCN")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.UCRCN)
                .IsRequired()
                .HasColumnName("UCRCN")
                .HasMaxLength(15)
                .IsUnicode(false);

            entity.Property(e => e.FACTLZCN)
               .HasColumnName("FACTLZCN")
               .HasColumnType("datetime")
               .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.UACTLZCN)
                .IsRequired()
                .HasColumnName("UACTLZCN")
                .HasMaxLength(15)
                .IsUnicode(false);

        }
    }
}
