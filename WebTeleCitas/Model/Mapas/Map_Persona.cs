﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Model.Entidad;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Mapas
{
    public class Map_Persona
    {
        public Map_Persona(EntityTypeBuilder<Persona> entity)
        {
            entity.HasKey(e => e.ID);

            entity.ToTable("GCSYS00", "CIT");

            entity.HasIndex(e => new { e.TDCMNTO, e.NDCMNTO })
                  .HasName("UQ_GCSYS00_00")
                  .IsUnique();

            entity.HasIndex(e => e.CRRO)
                  .HasName("UQ_GCSYS00_01")
                  .IsUnique();

            entity.Property(q => q.ID)
                .HasColumnName("ID")
                .ValueGeneratedOnAdd();

            entity.Property(e => e.PNOMBRE)
                //.IsRequired()
                .HasColumnName("PNOMBRE")
                .HasMaxLength(25)
                .IsUnicode(false);

            entity.Property(e => e.SNOMBRE)
                //.IsRequired()
                .HasColumnName("SNOMBRE")
                .HasMaxLength(25)
                .IsUnicode(false);

            entity.Property(e => e.APATERNO)
                .IsRequired()
                .HasColumnName("APATERNO")
                .HasMaxLength(30)
                .IsUnicode(false);

            entity.Property(e => e.AMATERNO)
                //.IsRequired()
                .HasColumnName("AMATERNO")
                .HasMaxLength(30)
                .IsUnicode(false);

            entity.Property(e => e.FNCMNTO)
                .IsRequired()
                .HasColumnName("FNCMNTO")
                .HasColumnType("datetime")
                .IsUnicode(false);

            entity.Property(e => e.SXO)
                .IsRequired()
                .HasColumnName("SXO")
                .HasMaxLength(1)
                .IsUnicode(false);

            entity.Property(e => e.TDCMNTO)
                .IsRequired()
                .HasColumnName("TDCMNTO")
                .HasMaxLength(1)
                .IsUnicode(false);

            entity.Property(e => e.NDCMNTO)
                .IsRequired()
                .HasColumnName("NDCMNTO")
                .HasMaxLength(15)
                .IsUnicode(false);

            entity.Property(e => e.NCLLR)
                //.IsRequired()
                .HasColumnName("NCLLR")
                .HasMaxLength(9)
                .IsUnicode(false);

            entity.Property(e => e.CRRO)
                //.IsRequired()
                .HasColumnName("CRRO")
                .HasMaxLength(100)
                .IsUnicode(false);

            entity.Property(e => e.CNTRSNA)
                //.IsRequired()
                .HasColumnName("CNTRSNA")
                .HasMaxLength(100)
                .IsUnicode(false);

            entity.Property(e => e.PSO)
                //.IsRequired()
                .HasColumnName("PSO")
                .HasMaxLength(3)
                .IsUnicode(false);

            entity.Property(e => e.ALTRA)
                //.IsRequired()
                .HasColumnName("ALTRA")
                .HasMaxLength(4)
                .IsUnicode(false);

            entity.Property(e => e.TSNGRE)
                //.IsRequired()
                .HasColumnName("TSNGRE")
                .HasMaxLength(5)
                .IsUnicode(false);



            entity.Property(e => e.CESTDO)
                .IsRequired()
                .HasColumnName("CESTDO")
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("('A')");

            entity.Property(e => e.FESTDO)
               .HasColumnName("FESTDO")
               .HasColumnType("datetime")
               .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.FCRCN)
                .HasColumnName("FCRCN")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.UCRCN)
                .IsRequired()
                .HasColumnName("UCRCN")
                .HasMaxLength(15)
                .IsUnicode(false);

            entity.Property(e => e.FACTLZCN)
               .HasColumnName("FACTLZCN")
               .HasColumnType("datetime")
               .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.UACTLZCN)
                .IsRequired()
                .HasColumnName("UACTLZCN")
                .HasMaxLength(15)
                .IsUnicode(false);

        }
    }
}
