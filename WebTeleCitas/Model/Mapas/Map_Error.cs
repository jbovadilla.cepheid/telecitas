﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Model.Entidad;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Mapas
{
    public class Map_Error
    {
        public Map_Error(EntityTypeBuilder<Error> entity)
        {
            entity.HasKey(e => e.ID);

            entity.ToTable("GCERR00", "CIT");

            entity.Property(e => e.ID)
                .HasColumnName("ID")
                .ValueGeneratedOnAdd();

            entity.Property(e => e.NTBLA)
                .IsRequired()
                .HasColumnName("NTBLA")
                .HasMaxLength(7)
                .IsUnicode(false);

            entity.Property(e => e.NPKGPD)
                .IsRequired()
                .HasColumnName("NPKGPD")
                .HasMaxLength(30)
                .IsUnicode(false);

            entity.Property(e => e.DSCRPCN)
                .IsRequired()
                .HasColumnName("DSCRPCN")
                .HasMaxLength(200)
                .IsUnicode(false);

            entity.Property(e => e.CESTDO)
                .IsRequired()
                .HasColumnName("CESTDO")
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("('A')");

            entity.Property(e => e.FESTDO)
               .HasColumnName("FESTDO")
               .HasColumnType("datetime")
               .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.FCRCN)
                .HasColumnName("FCRCN")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.UCRCN)
                .IsRequired()
                .HasColumnName("UCRCN")
                .HasMaxLength(15)
                .IsUnicode(false);

            entity.Property(e => e.FACTLZCN)
               .HasColumnName("FACTLZCN")
               .HasColumnType("datetime")
               .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.UACTLZCN)
                .IsRequired()
                .HasColumnName("UACTLZCN")
                .HasMaxLength(15)
                .IsUnicode(false);
        }
    }
}
