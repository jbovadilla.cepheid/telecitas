﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Model.Entidad;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Mapas
{
    public class Map_Paciente
    {
        public Map_Paciente(EntityTypeBuilder<Paciente> entity)
        {
            entity.HasKey(e => e.ID);

            entity.ToTable("GCSYS01", "CIT");

            entity.Property(e => e.ID)
                .HasColumnName("ID")
                .ValueGeneratedOnAdd();

            entity.Property(e => e.IDSYS00)
                .IsRequired()
                .HasColumnName("IDSYS00");

            entity.Property(e => e.IDPRNTSCO)
                .IsRequired()
                .HasColumnName("IDPRNTSCO")
                .HasMaxLength(1);

            entity.Property(e => e.NHCLNCA)
                //.IsRequired()
                .HasColumnName("NHCLNCA")
                .HasMaxLength(8)
                .IsUnicode(false);

            entity.Property(e => e.CESTDO)
                .IsRequired()
                .HasColumnName("CESTDO")
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("('A')");

            entity.Property(e => e.FESTDO)
               .HasColumnName("FESTDO")
               .HasColumnType("datetime")
               .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.FCRCN)
                .HasColumnName("FCRCN")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.UCRCN)
                .IsRequired()
                .HasColumnName("UCRCN")
                .HasMaxLength(15)
                .IsUnicode(false);

            entity.Property(e => e.FACTLZCN)
               .HasColumnName("FACTLZCN")
               .HasColumnType("datetime")
               .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.UACTLZCN)
                .IsRequired()
                .HasColumnName("UACTLZCN")
                .HasMaxLength(15)
                .IsUnicode(false);

            entity.Property(e => e.IDTTLR)
                //.IsRequired()
                .HasColumnName("IDTTLR");
        }
    }
}
