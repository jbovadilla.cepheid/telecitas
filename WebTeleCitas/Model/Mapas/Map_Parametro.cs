﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Model.Entidad;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Mapas
{
    public class Map_Parametro
    {
        public Map_Parametro(EntityTypeBuilder<Parametro> entity)
        {
            entity.HasKey(e => e.IPRMTRO);

            entity.ToTable("GCPAR00", "CIT");

            entity.Property(q => q.IPRMTRO)
                .HasColumnName("IPRMTRO")
                .ValueGeneratedOnAdd();

            entity.Property(e => e.NPRMTRO)
                .IsRequired()
                .HasColumnName("NPRMTRO")
                .HasMaxLength(25)
                .IsUnicode(false);

            entity.Property(e => e.DSCRPCN)
                .IsRequired()
                .HasColumnName("DSCRPCN")
                .HasMaxLength(250)
                .IsUnicode(false);

            entity.Property(e => e.ALS)
                .IsRequired()
                .HasColumnName("ALS")
                .HasMaxLength(25)
                .IsUnicode(false);

            entity.Property(e => e.VLR1)
                .IsRequired()
                .HasColumnName("VLR1")
                .HasMaxLength(250)
                .IsUnicode(false);

            entity.Property(e => e.VLR2)
                //.IsRequired()
                .HasColumnName("VLR2")
                .HasMaxLength(250)
                .IsUnicode(false);


            entity.Property(e => e.CESTDO)
                .IsRequired()
                .HasColumnName("CESTDO")
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("('A')");

            entity.Property(e => e.FESTDO)
               .HasColumnName("FESTDO")
               .HasColumnType("datetime")
               .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.FCRCN)
                .HasColumnName("FCRCN")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.UCRCN)
                .IsRequired()
                .HasColumnName("UCRCN")
                .HasMaxLength(15)
                .IsUnicode(false);

            entity.Property(e => e.FACTLZCN)
               .HasColumnName("FACTLZCN")
               .HasColumnType("datetime")
               .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.UACTLZCN)
                .IsRequired()
                .HasColumnName("UACTLZCN")
                .HasMaxLength(15)
                .IsUnicode(false);

        }
    }
}
