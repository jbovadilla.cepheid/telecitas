﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Entidad
{
    public class Error : Auditoria
    {
        public int ID { get; set; }
        public string NTBLA { get; set; }
        public string NPKGPD { get; set; }
        public string DSCRPCN { get; set; }
    }
}
