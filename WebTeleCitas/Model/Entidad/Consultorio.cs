﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Model.Entidad
{
    public class Consultorio
    {
        public int Id { get; set; }
        public int Piso { get; set; }
        [Column("DCNSLTRO")]
        public string DConsultorio { get; set; }
    }
}
