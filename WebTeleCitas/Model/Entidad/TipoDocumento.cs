﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Entidad
{
    public class TipoDocumento
    {
        public string Tipo { get; set; }
        public string Documento { get; set; }
    }
}
