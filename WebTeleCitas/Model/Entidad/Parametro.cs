﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Entidad
{
    public class Parametro : Auditoria
    {
        public int IPRMTRO { get; set; }
        public string NPRMTRO { get; set; }
        public string DSCRPCN { get; set; }
        public string ALS { get; set; }
        public string VLR1 { get; set; }
        public string VLR2 { get; set; }

    }
}
