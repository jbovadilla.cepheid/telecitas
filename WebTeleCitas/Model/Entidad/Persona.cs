﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Model.Entidad
{
    public class Persona : Auditoria
    {
        public int ID { get; set; }
        public string PNOMBRE { get; set; }
        public string SNOMBRE { get; set; }
        public string APATERNO { get; set; }
        public string AMATERNO { get; set; }
        public DateTime FNCMNTO { get; set; }
        public string SXO { get; set; }
        public string TDCMNTO { get; set; }
        [Required(ErrorMessage = "Ingrese su documento.")]
        public string NDCMNTO { get; set; }
        public string NCLLR { get; set; }
        public string CRRO { get; set; }
        public string CNTRSNA { get; set; }

        public string PSO { get; set; }
        public string ALTRA { get; set; }
        public string TSNGRE { get; set; }

        //cambioooo
    }
}
