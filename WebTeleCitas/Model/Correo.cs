﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;

namespace Model
{
    public class Correo
    {
        public string Envio2(string origen, string destino, string clave)
        {
            try
            {
                MailMessage oMailMessage = new MailMessage(origen, destino, "Cambio Contraseña", "link para cambio");

                oMailMessage.IsBodyHtml = true;

                SmtpClient oSmtpClient = new SmtpClient();
                oSmtpClient.EnableSsl = true;
                oSmtpClient.UseDefaultCredentials = false;
                oSmtpClient.Host = "smtp.gmail.com";
                oSmtpClient.Port = 587;
                oSmtpClient.Credentials = new System.Net.NetworkCredential(origen, clave);

                oSmtpClient.Send(oMailMessage);

                oSmtpClient.Dispose();
            }
            catch (Exception)
            {
                return "0";
            }
            

            return "1";
        }

        public string Envio(string destino, string codigo)
        {
            string origen = Codigos.CORREO;
            string clave = Codigos.CLAVE_CORREO;

            SmtpClient smtp = new SmtpClient();
            MailMessage correo = new MailMessage();

            try
            {

                correo.From = new MailAddress(origen);
                correo.To.Add(destino);
                //correo.Bcc.Add("");
                correo.Subject = "Asunto";
                correo.Body = "Ingrese este código para cambiar su contraseña : " + codigo;            
                correo.IsBodyHtml = false;
                correo.Priority = MailPriority.Normal;
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 25;
                smtp.Credentials = new
                      System.Net.NetworkCredential(origen, clave);
                smtp.EnableSsl = true;            
                smtp.Timeout = 600000;
            
                System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate (object s,
                        System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                        System.Security.Cryptography.X509Certificates.X509Chain chain,
                        System.Net.Security.SslPolicyErrors sslPolicyErrors)
                {
                    return true;
                };

            
                smtp.Send(correo);
            }
            catch (SmtpException ex)
            {
                throw ex;
            }
            

            return "1";
        }
    }
}
