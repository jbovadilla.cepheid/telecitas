﻿
using Microsoft.EntityFrameworkCore;
using Model.Entidad;
using Model.Mapas;

namespace Model
{
    public class CitasDbContext : DbContext
    {
        public CitasDbContext(DbContextOptions<CitasDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            new Map_Error(modelBuilder.Entity<Error>().ToTable("GCERR00", schema: "CIT"));
            new Map_Parametro(modelBuilder.Entity<Parametro>().ToTable("GCPAR00", schema: "CIT"));
            new Map_Persona(modelBuilder.Entity<Persona>().ToTable("GCSYS00", schema: "CIT"));
            new Map_Paciente(modelBuilder.Entity<Paciente>().ToTable("GCSYS01", schema: "CIT"));
            new Map_Especialidad(modelBuilder.Entity<Especialidad>().ToTable("GCSYS05", schema: "CIT"));
            new Map_Consultorio(modelBuilder.Entity<Consultorio>().ToTable("GCSYS06", schema: "CIT"));
            new Map_Aleatorio(modelBuilder.Entity<Aleatorio>().ToTable("GCSYS10", schema: "CIT"));

        }

        //public DbSet<Citas> Citas { get; set; }
        //public DbSet<Consultorio> Consultorio { get; set; }


    }
}
