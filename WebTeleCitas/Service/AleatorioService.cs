﻿using Model;
using Model.Crud;
using Model.Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Service
{
    public interface IAleatorioService
    {
        List<Aleatorio> GetAleatorios();
        Aleatorio GetAleatorio(int id);
        void InsertAleatorio(Aleatorio user);
        void UpdateAleatorio(Aleatorio user);
    }

    public class AleatorioService : IAleatorioService
    {
        private readonly ICrudAleatorio<Aleatorio> Crud_Ale;
        public AleatorioService(ICrudAleatorio<Aleatorio> _Crud_Ale)
        {
            this.Crud_Ale = _Crud_Ale;
        }

        public Aleatorio GetAleatorio(int id)
        {
            return Crud_Ale.Get(id);
        }

        public List<Aleatorio> GetAleatorios()
        {
            return Crud_Ale.GetAll().ToList();
        }

        public void InsertAleatorio(Aleatorio user)
        {
            user.FACTLZCN = DateTime.Now;
            user.FCRCN = DateTime.Now;
            user.FESTDO = DateTime.Now;
            user.UCRCN = Codigos.USUARIO;
            user.UACTLZCN = Codigos.USUARIO;
            user.CESTDO = Codigos.ESTADO_A;
            Crud_Ale.Insert(user);
        }

        public void UpdateAleatorio(Aleatorio user)
        {
            Crud_Ale.Update(user);
        }
    }
}
