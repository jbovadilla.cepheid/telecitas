﻿using Model;
using Model.Crud;
using Model.Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Service
{
    public interface IPacienteService
    {
        List<Paciente> GetPacientes();
        Paciente GetPaciente(int id);
        void InsertPaciente(Paciente user);
        void UpdatePaciente(Paciente user);

        Paciente GetPacienteXIdPersona(int idpersona);
        List<Paciente> GetPacientesXIdTitular(int idTitular);
    }

    public class PacienteService : IPacienteService
    {
        private readonly ICrudPaciente<Paciente> Crud_Paci;
        public PacienteService(ICrudPaciente<Paciente> _Crud_Paci)
        {
            this.Crud_Paci = _Crud_Paci;
        }

        public Paciente GetPaciente(int id)
        {
            return Crud_Paci.Get(id);
        }

        public List<Paciente> GetPacientes()
        {
            return Crud_Paci.GetAll().ToList();
        }

        public List<Paciente> GetPacientesXIdTitular(int idTitular)
        {
            return Crud_Paci.GetXIdTitular(idTitular).ToList();
        }

        public Paciente GetPacienteXIdPersona(int idpersona)
        {
            return Crud_Paci.GetXIdPersona(idpersona);
        }

        public void InsertPaciente(Paciente user)
        {
            user.FACTLZCN = DateTime.Now;
            user.FCRCN = DateTime.Now;
            user.FESTDO = DateTime.Now;
            user.UCRCN = Codigos.USUARIO;
            user.UACTLZCN = Codigos.USUARIO;
            user.CESTDO = Codigos.ESTADO_A;
            Crud_Paci.Insert(user);
        }

        public void UpdatePaciente(Paciente user)
        {
            Crud_Paci.Update(user);
        }
    }
}
