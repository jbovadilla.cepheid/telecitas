﻿using Microsoft.EntityFrameworkCore;
using Model;
using Model.Crud;
using Model.Entidad;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Service
{

    public interface IConsultorioService {
        /*void Create(Consultorio model);
        void Edit(Consultorio model);

        Consultorio Get(int id);
        List<Consultorio> Get_Sentencia(int id);
        List<Consultorio> Get_Procedure();
        */

        List<Consultorio> GetConsultorios();
        Consultorio GetConsultorio(int id);
        void InsertConsultorio(Consultorio user);
        void UpdateConsultorio(Consultorio user);


        void CreateWithProcedure(Consultorio user);


    }

    public class ConsultorioService : IConsultorioService
    {
      
        private readonly ICrudConsultorio<Consultorio> Crud_Mod;
        public ConsultorioService(ICrudConsultorio<Consultorio> _Crud_Mod)
        {
            this.Crud_Mod = _Crud_Mod;
        }

        public void CreateWithProcedure(Consultorio user)
        {
            Crud_Mod.CreatewithProcedure(user);
        }

        public Consultorio GetConsultorio(int id)
        {
            return Crud_Mod.Get(id);
        }

        public List<Consultorio> GetConsultorios()
        {
            return Crud_Mod.GetAll().ToList();
        }

        public void InsertConsultorio(Consultorio user)
        {
            Crud_Mod.Insert(user);
        }

        public void UpdateConsultorio(Consultorio user)
        {
            Crud_Mod.Update(user);
        }



        /*private readonly CitasDbContext _context;
      public ConsultorioService(CitasDbContext context)
      {
          _context = context;
      }*/

        /*
public void Create(Consultorio model)
{
   //1 forma aun sale error
   //var consultorio = _context.Add(new Consultorio { Piso = model.Piso,DConsultorio = model.DConsultorio});
   //_context.SaveChanges();
   //return consultorio.Entity;

   // _context.Consultorio.FromSql($"Execute cit.AGREGARCONSULTORIO @npiso,@cdecrip", parameters: new[] { model.Piso, model.DConsultorio });

   var piso = new SqlParameter("@npiso", model.Piso);
   var descrip = new SqlParameter("@cdecrip", model.DConsultorio);
   _context.Database.ExecuteSqlCommand("EXEC cit.AGREGARCONSULTORIO @npiso,@cdecrip",  piso,descrip );

}

public void Edit(Consultorio model)
{
   _context.Update(model);
   _context.SaveChanges();
}

public Consultorio Get(int id)
{
   return _context.Consultorio.Where(e => e.Id == id).FirstOrDefault();
}

public List<Consultorio> Get_Procedure()
{
   //var employees = _context.Consultorio.FromSql($"Execute cit.LISTACONSULTORIO {id}"); 
   var consultorio = _context.Consultorio.FromSql($"Execute cit.LISTACONSULTORIO").ToList();
   return consultorio;
}

public List<Consultorio> Get_Sentencia(int id)
{
   //var consultorio = _context.Consultorio.FromSql("SELECT * FROM CIT.GCSYS06 WHERE ID = @ID", new SqlParameter("@ID", id));
   var consultorio = _context.Consultorio.FromSql("SELECT * FROM CIT.GCSYS06").ToList();
   return consultorio;
}
*/

    }
}
