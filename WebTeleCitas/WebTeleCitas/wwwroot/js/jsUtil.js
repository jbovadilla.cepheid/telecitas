﻿function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function sLetras(e) {
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
    especiales = "8-37-39-46";

    tecla_especial = false
    for (var i in especiales) {
        if (key == especiales[i]) {
            tecla_especial = true;
            break;
        }
    }

    if (letras.indexOf(tecla) == -1 && !tecla_especial) {
        return false;
    }
}

function isTipoDocumento(oControl,dcontrol) {
    var tDoc = parseInt(oControl.value);
    var eTxtNDoc = document.getElementById(dcontrol);
    if (tDoc == 1) {
        eTxtNDoc.maxLength = 8;
        if (eTxtNDoc.value != "") {
            eTxtNDoc.value = eTxtNDoc.value.substring(0, 8);
            eTxtNDoc.classList.add("validate[required,minSize[8]]");
            eTxtNDoc.classList.remove("validate[required,minSize[15]]");
        }
    } else {
        eTxtNDoc.maxLength = 15;  
        eTxtNDoc.classList.add("validate[required,minSize[15]]");
        eTxtNDoc.classList.remove("validate[required,minSize[8]]");
    }
}

function dateFormat(el) {
    value = el.value;
    el.value = value.replace(/^([\d]{2})([\d]{2})([\d]{4})$/, "$1/$2/$3");
}
/*
function dateFormat2() {
    var date = document.getElementById('date');

    function checkValue(str, max) {
        if (str.charAt(0) !== '0' || str == '00') {
            var num = parseInt(str);
            if (isNaN(num) || num <= 0 || num > max) num = 1;
            str = num > parseInt(max.toString().charAt(0)) && num.toString().length == 1 ? '0' + num : num.toString();
        };
        return str;
    };

    date.addEventListener('input', function (e) {
        this.type = 'text';
        var input = this.value;
        if (/\D\/$/.test(input)) input = input.substr(0, input.length - 3);
        var values = input.split('/').map(function (v) {
            return v.replace(/\D/g, '')
        });
        if (values[0]) values[0] = checkValue(values[0], 12);
        if (values[1]) values[1] = checkValue(values[1], 31);
        var output = values.map(function (v, i) {
            return v.length == 2 && i < 2 ? v + ' / ' : v;
        });
        this.value = output.join('').substr(0, 14);
    });

    date.addEventListener('blur', function (e) {
        this.type = 'text';
        var input = this.value;
        var values = input.split('/').map(function (v, i) {
            return v.replace(/\D/g, '')
        });
        var output = '';

        if (values.length == 3) {
            var year = values[2].length !== 4 ? parseInt(values[2]) + 2000 : parseInt(values[2]);
            var month = parseInt(values[0]) - 1;
            var day = parseInt(values[1]);
            var d = new Date(year, month, day);
            if (!isNaN(d)) {
                document.getElementById('result').innerText = d.toString();
                var dates = [d.getMonth() + 1, d.getDate(), d.getFullYear()];
                output = dates.map(function (v) {
                    v = v.toString();
                    return v.length == 1 ? '0' + v : v;
                }).join(' / ');
            };
        };
        this.value = output;
    });

}*/