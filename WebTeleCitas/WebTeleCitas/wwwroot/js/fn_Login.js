﻿    function setRegistrar() {

        if ($('#FormRegister').validationEngine('validate')) {
            var tyc = "";
            if ($('#Chk_Tyc').is(":checked")) {
                tyc = "1";
            } else {
                tyc = "0";
            }

            var chkAceptaTssp = "";
            if ($('#Chk_Tssp').is(":checked")) {
                chkAceptaTssp = "1";
                //$('#TsspRegistroModal').modal("show");
            } else {
                chkAceptaTssp = "0";
            }

            var oparam = new Array();
            oparam[0] = $.trim($("#Txt_ApePat").val());
            oparam[1] = $.trim($("#Txt_ApeMat").val());
            oparam[2] = $.trim($("#Txt_Nombres").val());
            oparam[3] = $.trim($("#Txt_FecNac").val());
            oparam[4] = $.trim($("#Txt_Tel").val());
            oparam[5] = $("input:radio[name ='inlineRadioOptions']:checked").val();
            oparam[6] = $.trim($("#ddl_TipDoc").val());
            oparam[7] = $.trim($("#Txt_NumDoc").val());
            oparam[8] = $.trim($("#ddl_Sedes").val());
            oparam[9] = $.trim($("#Txt_Correo").val());
            oparam[10] = $.trim($("#Txt_Clave").val());
            oparam[11] = $.trim($("#Txt_RepClave").val());
            oparam[12] = tyc;

            var sendInfo = { oParametros: oparam };
            fc_CallService(JSON.stringify(sendInfo), PaginaActual + "/setRegistrar", function (res) {
                if (res.retorno > 0) {
                    //Cargar Modal Tssp
                    if (chkAceptaTssp == "1") {
                        $("#ContenidoModal").css("background-color", "#eee");
                        $('#TsspRegistroModal').modal("show");
                        $("#hdMensaje").val(res.msg_retorno);
                    } else {
                        fn_MensajeHref(res.msg_retorno, "Ingreso");
                        //fn_Limpiar();
                    }
                } else {
                    fn_MensajeError(res.msg_retorno);
                }
            });
        }
    }