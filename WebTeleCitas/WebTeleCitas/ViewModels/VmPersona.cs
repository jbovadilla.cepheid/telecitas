﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebTeleCitas.ViewModels
{
    public class VmPersona
    {
        public int ID { get; set; }
        public string PNOMBRE { get; set; }
        public string SNOMBRE { get; set; }
        public string APATERNO { get; set; }
        public string AMATERNO { get; set; }
        public DateTime FNCMNTO { get; set; }
        public string SXO { get; set; }
        public string TDCMNTO { get; set; }
        public string NDCMNTO { get; set; }
        public string NCLLR { get; set; }
        public string CRRO { get; set; }
        public string CNTRSNA { get; set; }

        public string PSO { get; set; }
        public string ALTRA { get; set; }
        public string TSNGRE { get; set; }

        public string EDAD { get; set; }
        public string SEXO { get; set; }
        
    }
}
