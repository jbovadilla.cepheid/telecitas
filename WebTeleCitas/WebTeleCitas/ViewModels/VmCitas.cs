﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebTeleCitas.ViewModels
{
    public class VmCitas
    {
        public List<VmFamiliar> listaDependiente { get; set; }
        public string IESPCLDD { get; set; }
    }
}
