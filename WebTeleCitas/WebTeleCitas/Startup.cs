﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Model;
using Model.Crud;
using Model.Entidad;
using Service;

namespace WebTeleCitas
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            //session
            services.AddSession();
            services.Configure<SecurityStampValidatorOptions>(options => options.ValidationInterval = TimeSpan.FromSeconds(10));
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options =>
                {
                    options.LoginPath = "/Persona/Login/";
                    options.AccessDeniedPath = "/Persona/Denegado/";
                    options.SlidingExpiration = true;
                    options.ExpireTimeSpan = TimeSpan.FromMinutes(10);
                });
            //fin

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            var sqlConnection = Configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<CitasDbContext>(options => options.UseSqlServer(sqlConnection));


            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();

            //registramos crud
            services.AddScoped(typeof(ICrud<>), typeof(Crud<>));
            services.AddScoped(typeof(ICrudConsultorio<Consultorio>), typeof(CrudConsultorio));
            services.AddScoped(typeof(ICrudPersona<Persona>), typeof(CrudPersona)); 
            services.AddScoped(typeof(ICrudAleatorio<Aleatorio>), typeof(CrudAleatorio));
            services.AddScoped(typeof(ICrudError<Error>), typeof(CrudError));
            services.AddScoped(typeof(ICrudParametro<Parametro>), typeof(CrudParametro));
            services.AddScoped(typeof(ICrudPaciente<Paciente>), typeof(CrudPaciente));
            services.AddScoped(typeof(ICrudEspecialidad<Especialidad>), typeof(CrudEspecialidad));

            //registramos los services
            services.AddTransient<IConsultorioService, ConsultorioService>();
            services.AddTransient<IPersonaService, PersonaService>();
            services.AddTransient<IAleatorioService, AleatorioService>();
            services.AddTransient<IErrorService, ErrorService>();
            services.AddTransient<IParametroService, ParametroService>();
            services.AddTransient<IPacienteService, PacienteService>();
            services.AddTransient<IEspecialidadService, EspecialidadService>();

        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseSession();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
