﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace WebTeleCitas.Controllers
{
    public class DependientesController : Controller
    {
        public IActionResult Index()
        {
            return View("../Pages/Dependientes/Index");
        }
    }
}