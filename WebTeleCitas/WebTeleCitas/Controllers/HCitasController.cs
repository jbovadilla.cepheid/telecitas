﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Model;
using Model.Entidad;
using Service;
using WebTeleCitas.ViewModels;

namespace WebTeleCitas.Controllers
{
    public class HCitasController : Controller
    {
        private readonly IPersonaService _servicePer;

        private readonly IHttpContextAccessor _httpContextAccessor;
        private ISession _session => _httpContextAccessor.HttpContext.Session;
        private readonly IErrorService _serviceError;

        public HCitasController(IPersonaService servicePer, IHttpContextAccessor httpContextAccessor, IErrorService serviceError)
        {
            this._servicePer = servicePer;
            _httpContextAccessor = httpContextAccessor;
            _serviceError = serviceError;
        }

        [HttpGet]
        public IActionResult Index()
        {
            //valida session
            var tdocumento = _session.GetString("tipodocumento");
            var documento = _session.GetString("documento");
            if (tdocumento == null || documento == null)
            { 
                return RedirectToAction("Login", "Persona");
            }
            //fin

            VmPersona permostrar = new VmPersona();
            try
            {
                Persona per = _servicePer.GetPersonasXTipNumero(tdocumento, documento);
                ViewBag.NOMBRE = per.PNOMBRE;
                
                permostrar.PNOMBRE = per.PNOMBRE;
                permostrar.ID = per.ID;
                HttpContext.Session.SetString("idpersona", per.ID.ToString());

                permostrar.SEXO = per.SXO == "M" ? "MASCULINO" : "FEMENINO";
                permostrar.EDAD = (DateTime.Today.AddTicks(-per.FNCMNTO.Ticks).Year - 1).ToString();
                permostrar.PSO = (per.PSO == null ? "0" : per.PSO);
                permostrar.ALTRA = (per.ALTRA == null ? "0" : per.ALTRA);
                permostrar.TSNGRE = (per.TSNGRE == null ? string.Empty : per.TSNGRE);


            }
            catch (Exception ex)
            {
                Errores("Persona", "Session", ex.Message.ToString());
                return RedirectToAction("Login", "Persona");
            }
            
            return View("../Pages/Inicio/Index", permostrar);
        }

        public void Errores(string tabla, string paquete, string descrip)
        {
            Error e = new Error();
            e.NTBLA = tabla;
            e.NPKGPD = paquete;
            e.DSCRPCN = descrip;
            _serviceError.InsertError(e);
        }

    }
}