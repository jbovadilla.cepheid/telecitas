﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Model.Entidad;
using Service;
using WebTeleCitas.Models;

namespace WebTeleCitas.Controllers
{
    public class HomeController : Controller
    {
        //private readonly IConfiguration configuration;
        private readonly IConsultorioService _serviceConsul;

        public HomeController( IConsultorioService serviceConsul)
        {
            //this.configuration = config;
            this._serviceConsul = serviceConsul;
        }

        /*
        public List<Consultorio> GetAllConsultorio()
        {
            List<Consultorio> list = new List<Consultorio>();
            string cnn = configuration.GetConnectionString("DefaultConnection");
            using (SqlConnection conn = new SqlConnection(cnn))
            {
                conn.Open();
                //string sql = "select * from CIT.GCSYS06";
                string sql = "cit.LISTACONSULTORIO";
                SqlCommand cmd = new SqlCommand(sql, conn);

                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new Consultorio()
                        {
                            Id = Convert.ToInt32(reader["Id"]),
                            Piso = Convert.ToInt32(reader["Piso"]),
                            DConsultorio = reader["dcnsltro"].ToString()
                        });
                    }
                }
            }
            return list;
        }*/

        [HttpGet]
        public IActionResult Index()
        {
            return View(_serviceConsul.GetConsultorios());
            //return View(_serviceConsul.Get_Procedure());
            //return View(GetAllConsultorio());
        }

        [HttpGet]
        public IActionResult Privacy()
        {
            return View();
        }

        [HttpPost]
        public IActionResult AgregarEditar(Consultorio model)
        {
            _serviceConsul.InsertConsultorio(model);
            return View("/Home/Privacy");
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
