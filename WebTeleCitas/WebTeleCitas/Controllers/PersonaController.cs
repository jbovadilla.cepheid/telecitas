﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Model;
using Model.Entidad;
using Service;
using WebTeleCitas.ViewModels;

namespace WebTeleCitas.Controllers
{
    public class PersonaController : Controller
    {
        private readonly IPersonaService _servicePer;
        private readonly IAleatorioService _serviceAle;
        private readonly IPacienteService _servicePaci;
        private readonly IErrorService _serviceError;

        public PersonaController(IPersonaService servicePer, IAleatorioService serviceAle, IPacienteService servicePaci, IErrorService serviceError)
        {
            this._servicePer = servicePer;
            this._serviceAle = serviceAle;
            this._servicePaci = servicePaci;
            this._serviceError = serviceError;
        }

        public void Errores(string tabla, string paquete, string descrip)
        {
            Error e = new Error();
            e.NTBLA = tabla;
            e.NPKGPD = paquete;
            e.DSCRPCN = descrip;
            _serviceError.InsertError(e);
            //return RedirectToAction(action, "Error", e);
        }

        #region Persona
        [HttpGet]
        public IActionResult Index()
        {
            //ViewBag.Mensaje = null;
            return View();
        }

        [HttpGet]
        public IActionResult EditarPersona(VmPersona model)
        {
            return PartialView("EditarPersona", model);
        }
        [HttpPost]
        public IActionResult ActualizarPersona(VmPersona model)
        {

            Persona per = _servicePer.GetPersona(model.ID);
            per.PSO = model.PSO;
            per.ALTRA = model.ALTRA;
            per.TSNGRE = model.TSNGRE;
            per.CNTRSNA = Contrasena.DesEncriptar(per.CNTRSNA);
            _servicePer.UpdatePersona(per);

            //return View("../Pages/Inicio/Index", model);
            return RedirectToAction("Index", "HCitas");
        }


        [HttpPost]
        public IActionResult AgregarPersona(Persona model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            ViewBag.Mensaje = null;

            //validacion tipo y numero documento
            if ((_servicePer.GetPersonasXTipNumero(model.TDCMNTO, model.NDCMNTO)) != null)
            {
                ViewBag.Mensaje = Mensajes.TIPO_DOCUMENTO_EXISTE;
                return View("Index");
            }

            //validacion correo
            if ((_servicePer.GetPersonasXCorreo(model.CRRO)) != null)
            {
                ViewBag.Mensaje = Mensajes.CORREO_EXISTE;
                return View("Index");
            }

            //validacion edad
            if (!(new Validacion().CalcularEdad(model.FNCMNTO)))
            {
                ViewBag.Mensaje = Mensajes.MENOR_EDAD;
                return View("Index");
                //ModelState.AddModelError("", "Hubo un error al insertar el cliente");
            }

            try
            {

                model.PNOMBRE = model.PNOMBRE.ToUpper();
                model.SNOMBRE = model.SNOMBRE.ToUpper();
                model.APATERNO = model.APATERNO.ToUpper();
                model.AMATERNO = model.AMATERNO.ToUpper();

                //registrar persona
                _servicePer.InsertPersona(model);

                //registrar paciente
                Paciente pa = new Paciente();
                pa.IDSYS00 = model.ID;
                pa.IDPRNTSCO = Codigos.CODTITULAR;
                pa.NHCLNCA = null;
                pa.IDTTLR = 0;
                _servicePaci.InsertPaciente(pa);

                //mensaje
                ViewBag.MensajeR = Mensajes.REGISTRO_CORRECTO;
                //return View("Login", model);
                
            }
            catch (Exception ex)
            {
                Errores("Persona", "Insertar", ex.InnerException.Message.ToString());
                return View("Index");
            }

            return View("Index");

        }
        #endregion
        #region Login
        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public IActionResult IniciarSession(Persona model)
        {
            try
            {
                //validacion tipo y numero documento(usuario)
                if ((_servicePer.GetPersonasXTipNumero(model.TDCMNTO, model.NDCMNTO)) == null)
                {
                    ViewBag.Mensaje = Mensajes.TIPO_DOCUMENTO_INCORRECTO;
                    return View("Login");
                }

                //validacion de contraseña
                Persona per = _servicePer.GetPersonaSession(model.TDCMNTO, model.NDCMNTO, model.CNTRSNA);
                if (per == null)
                {
                    ViewBag.Mensaje = Mensajes.CONTRASENA_INCORRECTA;
                    return View("Login");
                }
                else
                {
                    //valida se encuentra activo
                    if (per.CESTDO != Codigos.ESTADO_A)
                    {
                        ViewBag.Mensaje = Mensajes.PERSONA_INACTIVO;
                        return View("Login");
                    }
                }

                HttpContext.Session.SetString("tipodocumento", model.TDCMNTO);
                HttpContext.Session.SetString("documento", model.NDCMNTO);
            }
            catch (Exception ex)
            {
                Errores("Persona", "Session", ex.Message.ToString());
                return View("Login");
            }

            //return View("../Pages/Inicio/Index");
            return RedirectToAction("Index", "HCitas");
        }
        #endregion
        #region Clave
        [HttpGet]
        public IActionResult Clave()
        {
            return View();
        }
        
        [HttpPost]
        public IActionResult CorreoCambioClave(Persona model)
        {
            ViewBag.MensajeE = null;

            Persona per = _servicePer.GetPersonasXCorreo(model.CRRO);

            //validacion correo
            if (per  == null)
            {
                ViewBag.Mensaje = Mensajes.CORREO_NO_EXISTE;
                return View("Clave");
            }

            Random generator = new Random();
            int random = generator.Next(100000, 999999);
            try
            {
                string rpta = new Correo().Envio(model.CRRO, random.ToString());

                if (rpta == "1")
                {
                   
                    //tabla de codigo aleatorios
                    Aleatorio ale = new Aleatorio();
                    ale.CPRSNA = per.ID;
                    ale.ALTRO = random;
                    _serviceAle.InsertAleatorio(ale);
                    ViewBag.MensajeE = Mensajes.CORREO_ENVIADO;
                   // return View("CambioClave");
                }
                
            }
            catch (Exception ex)
            {
                Errores("Persona", "EnvioCorreo", ex.InnerException.Message.ToString());
                return View("Clave");
            }

            return View("Clave");
        }
        [HttpGet]
        public IActionResult CambioClave()
        {
            return View();
        }
        [HttpPost]
        public IActionResult CambioClave(string aleatorio, string contrasena, string confirmar)
        {
            try
            {
                Aleatorio ale = _serviceAle.GetAleatorios().Where(x => x.ALTRO == int.Parse(aleatorio)).FirstOrDefault();

                if (ale == null)
                {
                    ViewBag.Mensaje = Mensajes.ALEATORIO_NO_EXISTE;
                    return View();
                }

                Persona per = _servicePer.GetPersona(ale.CPRSNA);
                per.CNTRSNA = contrasena;
                _servicePer.UpdatePersona(per);
                ViewBag.MensajeC = Mensajes.CONTRASENA_MODIFICADA;

            }
            catch (Exception ex)
            {
                Errores("Persona", "CambioClave", ex.Message.ToString());
                return View();
            }

            return View();
        }
        #endregion
        }
}
