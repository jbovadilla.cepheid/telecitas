﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Model.Entidad;
using Service;

namespace WebTeleCitas.Controllers
{
    public class ErrorController : Controller
    {
        private readonly IErrorService _serviceErr;

        public ErrorController(IErrorService serviceErr)
        {
            this._serviceErr = serviceErr;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult InsertarErrorPersona(Error model)
        {
            _serviceErr.InsertError(model);
            ViewBag.Mensaje = model.DSCRPCN;
            //return View("../Persona/Index");
           return RedirectToAction("Index", "Persona");
        }

    }
}