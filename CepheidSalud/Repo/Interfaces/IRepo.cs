﻿using Models;
using System.Linq;

namespace Repo.Interfaces
{
    public interface IRepo<T> where T : Auditoria
    {
        IQueryable<T> GetAll();
        T Get(decimal id);
        void Insert(T entity);
        void InsertWithTransaction(T entity);
        void Update(T entity);
        void Commit();
    }
    public interface IRepoSistema<T> where T : Sistema
    {
        IQueryable<T> GetAll();
        T Get(int id);
        void Insert(T entity);
        void InsertWithTransaction(T entity);
        void Update(T entity);
        void Commit();
    }
    public interface IRepoEmpresa<T> where T : Empresa
    {
        IQueryable<T> GetAll();
        T Get(int id);
        void Insert(T entity);
        void InsertWithTransaction(T entity);
        void Update(T entity);
        void Commit();
    }
    //public interface IRepoErrPersona<T> where T : ErrorPersona
    //{
    //    IQueryable<T> GetAll();
    //    void Insert(T entity);
    //    void InsertWithTransaction(T entity);
    //    void Commit();
    //}
    public interface IRepoErrSeguridad<T> where T : ErrorSeguridad
    {
        IQueryable<T> GetAll();
        void Insert(T entity);
        void InsertWithTransaction(T entity);
        void Commit();
    }
}
