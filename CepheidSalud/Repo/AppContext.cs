﻿using Microsoft.EntityFrameworkCore;
using Models;
using Models.Map;
using System.Linq;

namespace Repo
{
    public class AppContext : DbContext
    {
        
        public AppContext(DbContextOptions<AppContext> options) : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            ///esto se hace por cada entidad,
            ///Cada entidad tiene un map y hace referencia a una tabla 
            #region Persona
            /*new Map_ErrorPersona(modelBuilder.Entity<ErrorPersona>().ToTable("PERERR00", schema: "PER"));
            new Map_Persona(modelBuilder.Entity<Persona>().ToTable("PERPER00", schema: "PER"));
            new Map_TipoDocumento(modelBuilder.Entity<TipoDocumento>().ToTable("PERPER01", schema: "PER"));
            new Map_Direccion(modelBuilder.Entity<Direccion>().ToTable("PERPER02", schema: "PER"));
            new Map_Telefono(modelBuilder.Entity<Telefono>().ToTable("PERPER03", schema: "PER"));
            new Map_Correo(modelBuilder.Entity<Correo>().ToTable("PERPER04", schema: "PER"));
            new Map_Firma(modelBuilder.Entity<Firma>().ToTable("PERPER05", schema: "PER"));
            new Map_Foto(modelBuilder.Entity<Foto>().ToTable("PERPER06", schema: "PER"));
            new Map_RepreLegal(modelBuilder.Entity<RepresentanteLegal>().ToTable("PERPER07", schema: "PER"));*/
            #endregion
            #region Parametros
            /*new Map_ParametroEmpresa(modelBuilder.Entity<ParametroEmpresa>().ToTable("PARPAR00", schema: "PAR"));
            new Map_ParametroSucursal(modelBuilder.Entity<ParametroSucursal>().ToTable("PARPAR01", schema: "PAR"));
            new Map_ParametroSistema(modelBuilder.Entity<ParametroSistema>().ToTable("PARPAR02", schema: "PAR"));
            new Map_GrupoDato(modelBuilder.Entity<GrupoDato>().ToTable("PARPAR03", schema: "PAR"));
            new Map_GrupoDatoDetalle(modelBuilder.Entity<GrupoDatoDetalle>().ToTable("PARPAR04", schema: "PAR"));
            new Map_ModuloGrupoDato(modelBuilder.Entity<ModuloGrupoDato>().ToTable("PARPAR05", schema: "PAR"));
            new Map_ModuloGrupoDatoDetalle(modelBuilder.Entity<ModuloGrupoDatoDetalle>().ToTable("PARPAR06", schema: "PAR"));*/
            #endregion
            #region PlanBeneficio
            ///mantenimientos
            /*new Map_Diagnostico(modelBuilder.Entity<Diagnostico>().ToTable("SALMAN00", schema: "SAL"));
            new Map_MaestroGrupoBeneficio(modelBuilder.Entity<MaestroGrupoBeneficio>().ToTable("SALMAN01", schema: "SAL"));
            new Map_Beneficio(modelBuilder.Entity<Beneficio>().ToTable("SALMAN02", schema: "SAL"));
            new Map_Procedimiento(modelBuilder.Entity<Procedimiento>().ToTable("SALMAN03", schema: "SAL"));
            new Map_Segus(modelBuilder.Entity<Segus>().ToTable("SALMAN04", schema: "SAL"));
            ///planes beneficios
            new Map_PlanBeneficio(modelBuilder.Entity<PlanBeneficio>().ToTable("SALPBE00", schema: "SAL"));
            new Map_VersionPlanBeneficio(modelBuilder.Entity<VersionPlanBeneficio>().ToTable("SALPBE01", schema: "SAL"));
            new Map_DiagnosticoExcluido(modelBuilder.Entity<DiagnosticoExcluido>().ToTable("SALPBE02", schema: "SAL"));
            new Map_FormaReclamo(modelBuilder.Entity<FormaReclamo>().ToTable("SALPBE03", schema: "SAL"));
            new Map_PlanBeneficioFormaReclamo(modelBuilder.Entity<PlanBeneficioFormaReclamo>().ToTable("SALPBE04", schema: "SAL"));
            new Map_Copago(modelBuilder.Entity<Copago>().ToTable("SALPBE05", schema: "SAL"));
            new Map_CantidadAportesPersona(modelBuilder.Entity<CantidadAportesPersona>().ToTable("SALPBE06", schema: "SAL"));
            new Map_CostoVersionPlan(modelBuilder.Entity<CostoVersionPlan>().ToTable("SALPBE07", schema: "SAL"));
            #endregion
            #region Proveedor
            new Map_Proveedor(modelBuilder.Entity<Proveedor>().ToTable("SALPRO00", schema: "SAL"));
            new Map_ProveedorProcedimientoSegus(modelBuilder.Entity<ProveedorProcedimientoSegus>().ToTable("SALPRO01", schema: "SAL"));
            new Map_ProveedorSucursal(modelBuilder.Entity<ProveedorSucursal>().ToTable("SALPRO02", schema: "SAL"));
            new Map_GrupoBeneficio(modelBuilder.Entity<GrupoBeneficio>().ToTable("SALPRO03", schema: "SAL"));
            new Map_ContratoProveedor(modelBuilder.Entity<ContratoProv>().ToTable("SALCPR00", schema: "SAL"));*/
            #endregion
            #region Seguridad
            new Map_ErrorSeguridad(modelBuilder.Entity<ErrorSeguridad>().ToTable("SEGERR00", schema: "SEG"));
            ///Seguridad
            new Map_Empresa(modelBuilder.Entity<Empresa>().ToTable("SEGSEG00", schema: "SEG"));
            new Map_Sucursal(modelBuilder.Entity<Sucursal>().ToTable("SEGSEG01", schema: "SEG"));
            new Map_Usuario(modelBuilder.Entity<Usuario>().ToTable("SEGSEG02", schema: "SEG"));
            new Map_EmpresaSucursalUsuario(modelBuilder.Entity<EmpresaSucursalUsuario>().ToTable("SEGSEG03", schema: "SEG"));
            new Map_UsuarioPerfil(modelBuilder.Entity<UsuarioPerfil>().ToTable("SEGSEG04", schema: "SEG"));
            ///Sisteema
            new Map_Sistema(modelBuilder.Entity<Sistema>().ToTable("SEGSIS00", schema: "SEG"));
            new Map_Modulo(modelBuilder.Entity<Modulo>().ToTable("SEGSIS01", schema: "SEG"));
            new Map_Perfil(modelBuilder.Entity<Perfil>().ToTable("SEGSIS02", schema: "SEG"));
            new Map_Opcion(modelBuilder.Entity<Opcion>().ToTable("SEGSIS03", schema: "SEG"));
            new Map_OpcionDetalle(modelBuilder.Entity<OpcionDetalle>().ToTable("SEGSIS04", schema: "SEG"));
            new Map_OpcionPerfil(modelBuilder.Entity<OpcionPerfil>().ToTable("SEGSIS05", schema: "SEG"));
            new Map_OpcionPerfilDetalle(modelBuilder.Entity<OpcionPerfilDetalle>().ToTable("SEGSIS06", schema: "SEG"));
            #endregion

            modelBuilder.Entity<Empresa>().Ignore(b => b.nId);
            modelBuilder.Entity<ErrorSeguridad>().Ignore(b => b.nId);
            modelBuilder.Entity<Sistema>().Ignore(b => b.nId);
            //modelBuilder.Entity<ErrorPersona>().Ignore(b => b.nId);
        }
    }
}
