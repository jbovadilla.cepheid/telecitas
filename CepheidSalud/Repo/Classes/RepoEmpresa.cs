﻿using Microsoft.EntityFrameworkCore;
using Models;
using Repo.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Repo.Classes
{
    public class RepoEmpresa : IRepoEmpresa<Empresa>
    {
        private readonly AppContext context;
        private DbSet<Empresa> entities;

        string errorMessage = string.Empty;

        public RepoEmpresa(AppContext context)
        {
            this.context = context;
            entities = context.Set<Empresa>();
        }
        public IQueryable<Empresa> GetAll()
        {
            return entities;
        }

        public Empresa Get(int id)
        {
            return entities.SingleOrDefault(s => s.IEmprsa == id);
        }
        public void Insert(Empresa entity)
        {
            try
            {
                if (entity == null)
                {
                    throw new ArgumentNullException("entity");
                }
                entities.Add(entity);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void InsertWithTransaction(Empresa entity)
        {
            try
            {
                if (entity == null)
                {
                    throw new ArgumentNullException("entity");
                }
                entities.Add(entity);
                //context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Update(Empresa entity)
        {
            try
            {
                if (entity == null)
                {
                    throw new ArgumentNullException("entity");
                }
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Commit()
        {
            try
            {
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
