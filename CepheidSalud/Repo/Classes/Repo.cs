﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Models;
using Repo.Interfaces;
using System;
using System.Linq;

namespace Repo.Classes
{
    public class Repository<T> : IRepo<T> where T : Auditoria
    {
        private readonly AppContext context;
        private DbSet<T> entities;

        string errorMessage = string.Empty;

        public Repository(AppContext context)
        {
            this.context = context;
             entities = context.Set<T>();
        }

        public IQueryable<T> GetAll()
        {
            return entities;
        }

        public T Get(decimal id)
        {
            return entities.SingleOrDefault(s => s.nId == id);
        }
        public void Insert(T entity)
        {
            try
            {
                if (entity == null)
                {
                    throw new ArgumentNullException("entity");
                }
                entities.Add(entity);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void InsertWithTransaction(T entity)
        {
            try
            {
                if (entity == null)
                {
                    throw new ArgumentNullException("entity");
                }
                entities.Add(entity);
                //context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Update(T entity)
        {
            try
            {
                if (entity == null)
                {
                    throw new ArgumentNullException("entity");
                }
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
      
        public void Commit()
        {
            try
            {
                 context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void RollBack()
        {
            try
            {
                //context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IDbContextTransaction BeginTransaction()
        {
            try
            {
                return context.Database.BeginTransaction();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
