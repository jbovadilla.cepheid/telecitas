﻿using Models;
using System.Collections.Generic;

namespace Services.Interfaces
{
    public interface IServ_OpcionPerfil
    {
        List<OpcionPerfil> GetOpcionPerfils();
        List<OpcionPerfil> ObtenerOpcionPerfilXModuloXSistema(decimal id, decimal idM);
        OpcionPerfil GetOpcionPerfil(long id);
        void InsertOpcionPerfil(OpcionPerfil user);
        void InsertOpcionPerfil(List<OpcionPerfil> user, List<OpcionPerfilDetalle> list);
        void UpdateOpcionPerfil(OpcionPerfil user);
    }

}
