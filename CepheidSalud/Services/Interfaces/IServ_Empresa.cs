﻿using Models;
using System.Collections.Generic;

namespace Services.Interfaces
{
    public interface IServ_Empresa
    {
        List<Empresa> GetEmpresas();
        Empresa GetEmpresa(int id);
        void InsertEmpresa(Empresa user);
        void UpdateEmpresa(Empresa user);
    }
}
