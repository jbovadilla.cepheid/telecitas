﻿using Models;
using System.Collections.Generic;

namespace Services.Interfaces
{
    public interface IServ_OpcionPerfilDetalle
    {
        List<OpcionPerfilDetalle> GetOpcionPerfilDetalles();
        List<OpcionPerfilDetalle> ObtenerOpcionPerfilDetalleXModuloXSistema(decimal id, decimal idM,decimal idopc);
        OpcionPerfilDetalle GetOpcionPerfilDetalle(long id);
        void InsertOpcionPerfilDetalle(OpcionPerfilDetalle user);
        void UpdateOpcionPerfilDetalle(OpcionPerfilDetalle user);
    }
}
