﻿using Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Interfaces
{
    public interface IServ_Sistema
    {
        List<Sistema> GetSistemas();
        Sistema GetSistema(int id);
        void InsertSistema(Sistema user);
        void UpdateSistema(Sistema user);
    }
}
