﻿using System;
using System.Collections.Generic;
using System.Text;
using Models;

namespace Services.Interfaces
{
    public interface IServ_Opcion
    {
        List<Opcion> GetOpcions();
        List<Opcion> ObtenerOpcionXModuloXSistema(decimal id, decimal idM);
        Opcion GetOpcion(long id);
        void InsertOpcion(Opcion user);
        void UpdateOpcion(Opcion user);
    }
}
