﻿using Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Interfaces
{
   public interface IServ_UsuarioPerfil
    {
        List<UsuarioPerfil> GetUsuarioPerfils();
        UsuarioPerfil GetUsuarioPerfil(long id);
        void InsertUsuarioPerfil(UsuarioPerfil user);
        void UpdateUsuarioPerfil(UsuarioPerfil user);
    }
}
