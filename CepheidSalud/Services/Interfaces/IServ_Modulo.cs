﻿using Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Interfaces
{
    public interface IServ_Modulo
    {
        List<Modulo> GetModulos();
        List<Modulo> ObtenerModuloXSistema(decimal id);
        Modulo GetModulo(decimal id);
        void InsertModulo(Modulo user);
        void UpdateModulo(Modulo user);
    }
}
