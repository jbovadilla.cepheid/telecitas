﻿using Microsoft.EntityFrameworkCore;
using Models;
using Repo.Interfaces;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Services.Classes
{
    public class Serv_Opcion : IServ_Opcion, IServ_OpcionPerfil, IServ_OpcionPerfilDetalle
    {
        private IRepo<Opcion> Repo_Opc;
        private IRepo<OpcionPerfil> Repo_OpcPer;
        private IRepo<OpcionPerfilDetalle> Repo_OpcPerDet;

        public Serv_Opcion(IRepo<Opcion> Repo_Opc, IRepo<OpcionPerfil> Repo_OpcPer, IRepo<OpcionPerfilDetalle> Repo_OpcPerDet)
        {
            this.Repo_Opc = Repo_Opc;
            this.Repo_OpcPer = Repo_OpcPer;
            this.Repo_OpcPerDet = Repo_OpcPerDet;
        }

        public List<Opcion> GetOpcions()
        {
            return Repo_Opc.GetAll()
                .Include(x => x.OpcionDetalle).ToList();
        }

        public Opcion GetOpcion(long id)
        {
            return Repo_Opc.Get(id);
        }

        public void InsertOpcion(Opcion user)
        {
            user.IOpcn = ObtenerOpcionXModuloXSistema(user.ISstma, user.IMdlo).Count() + 1;
            Repo_Opc.Insert(user);
        }
        public void UpdateOpcion(Opcion user)
        {
            Repo_Opc.Update(user);
        }

        public List<Opcion> ObtenerOpcionXModuloXSistema(decimal id, decimal idM)
        {
            return GetOpcions().Where(x => x.ISstma == id && x.IMdlo == idM).ToList();
        }

        public List<OpcionPerfil> GetOpcionPerfils()
        {
            return Repo_OpcPer.GetAll()
                .Include(d => d.Perfil)
                .Include(x => x.Opcion)
                    .ThenInclude(y => y.Modulo)
                .Include(h => h.OpcionPerfilDetalle)
                .ToList();
        }

        public List<OpcionPerfil> ObtenerOpcionPerfilXModuloXSistema(decimal id, decimal idM)
        {
            return GetOpcionPerfils().Where(x => x.ISstma == id && x.IMdlo == idM).ToList();
        }

        public OpcionPerfil GetOpcionPerfil(long id)
        {
            return Repo_OpcPer.Get(id);
        }

        public void InsertOpcionPerfil(OpcionPerfil user)
        {
            Repo_OpcPer.Insert(user);
        }
        public void InsertOpcionPerfil(List<OpcionPerfil> opciones, List<OpcionPerfilDetalle> detalles)
        {
            try
            {
                foreach (var item in opciones)
                {
                    var cont = GetOpcionPerfils().FirstOrDefault(x => x.ISstma == item.ISstma && x.IMdlo == item.IMdlo && x.IPrfl == item.IPrfl && x.IOpcn == item.IOpcn);
                    if (cont == null)
                    {
                        InsertOpcionPerfil(item);
                    }
                    else
                    {
                        cont.UActlzcn = item.UActlzcn;
                        cont.CEscrtra = item.CEscrtra;
                        cont.CEstdo = item.CEstdo;
                        cont.CHbltdo = item.CHbltdo;
                        cont.CMdfccn = item.CMdfccn;
                        cont.FActlzcn = item.FActlzcn;
                        cont.FEstdo = item.FEstdo;
                        UpdateOpcionPerfil(cont);
                    }
                }
                foreach (var item in detalles)
                {
                    var cont = GetOpcionPerfilDetalles().FirstOrDefault(x => x.ISstma == item.ISstma && x.IMdlo == item.IMdlo && x.IPrfl == item.IPrfl && x.IOpcn == item.IOpcn && x.IODtlle == item.IODtlle);
                    if (cont == null)
                    {
                        InsertOpcionPerfilDetalle(item);
                    }
                    else
                    {
                        cont.UActlzcn = item.UActlzcn;
                        cont.CEscrtra = item.CEscrtra;
                        cont.CEstdo = item.CEstdo;
                        cont.CHbltdo = item.CHbltdo;
                        cont.CMdfccn = item.CMdfccn;
                        cont.FActlzcn = item.FActlzcn;
                        cont.FEstdo = item.FEstdo;
                        UpdateOpcionPerfilDetalle(item);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void UpdateOpcionPerfil(OpcionPerfil user)
        {
            Repo_OpcPer.Update(user);
        }

        public List<OpcionPerfilDetalle> GetOpcionPerfilDetalles()
        {
            return Repo_OpcPerDet.GetAll().ToList();
        }

        public OpcionPerfilDetalle GetOpcionPerfilDetalle(long id)
        {
            return Repo_OpcPerDet.Get(id);
        }

        public void InsertOpcionPerfilDetalle(OpcionPerfilDetalle user)
        {
            user.IOpcn = ObtenerOpcionPerfilDetalleXModuloXSistema(user.ISstma, user.IMdlo, user.IOpcn).Count() + 1;
            Repo_OpcPerDet.Insert(user);
        }
        public void UpdateOpcionPerfilDetalle(OpcionPerfilDetalle user)
        {
            Repo_OpcPerDet.Update(user);
        }

        public List<OpcionPerfilDetalle> ObtenerOpcionPerfilDetalleXModuloXSistema(decimal id, decimal idM, decimal idOpDet)
        {
            return GetOpcionPerfilDetalles().Where(x => x.ISstma == id && x.IMdlo == idM && x.IOpcn == idOpDet).ToList();
        }
    }
}
