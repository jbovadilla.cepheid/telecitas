﻿using Microsoft.EntityFrameworkCore;
using Models;
using Repo.Interfaces;
using Services.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Services.Classes
{
    public class Serv_Modulo : IServ_Modulo
    {
        private IRepo<Modulo> Repo_Mod;

        public Serv_Modulo(IRepo<Modulo> Repo_Mod)
        {
            this.Repo_Mod = Repo_Mod;
        }

        public Modulo GetModulo(decimal id)
        {
            return Repo_Mod.Get(id);
        }

        public List<Modulo> GetModulos()
        {
            return Repo_Mod.GetAll()
                .Include(x => x.Opcion)
                .ToList();
        }

        public void InsertModulo(Modulo mod)
        {
            mod.IMdlo = ObtenerModuloXSistema(mod.ISstma).Count() + 1;
            Repo_Mod.Insert(mod);
        }

        public List<Modulo> ObtenerModuloXSistema(decimal idSis)
        {
            return GetModulos().Where(x => x.ISstma == idSis).ToList();
        }

        public void UpdateModulo(Modulo user)
        {
            Repo_Mod.Update(user);
        }
    }
}
