﻿using System;
using System.Collections.Generic;
using System.Text;
using Models;
using Services.Interfaces;
using Repo.Interfaces;
using System.Linq;

namespace Services.Classes
{
    public class Serv_Sistema : IServ_Sistema
    {
        private IRepoSistema<Sistema> Repo_Sis;

        public Serv_Sistema(IRepoSistema<Sistema> Repo_Sis)
        {
            this.Repo_Sis = Repo_Sis;
        }

        public Sistema GetSistema(int id)
        {
            return Repo_Sis.Get(id);
        }

        public List<Sistema> GetSistemas()
        {
            return Repo_Sis.GetAll().ToList();
        }

        public void InsertSistema(Sistema user)
        {
            Repo_Sis.Insert(user);
        }

        public void UpdateSistema(Sistema user)
        {
            Repo_Sis.Update(user);
        }
    }
}
