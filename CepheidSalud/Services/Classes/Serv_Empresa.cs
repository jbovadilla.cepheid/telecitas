﻿using Models;
using Repo.Interfaces;
using Services.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Services.Classes
{
    public class Serv_Empresa : IServ_Empresa
    {
        private IRepoEmpresa<Empresa> Repo_Emp;

        public Serv_Empresa(IRepoEmpresa<Empresa> Repo_Emp)
        {
            this.Repo_Emp = Repo_Emp;
        }

        public List<Empresa> GetEmpresas()
        {
            return Repo_Emp.GetAll().ToList();
        }

        public Empresa GetEmpresa(int id)
        {
            return Repo_Emp.Get(id);
        }

        public void InsertEmpresa(Empresa user)
        {
            Repo_Emp.Insert(user);
        }
        public void UpdateEmpresa(Empresa user)
        {
            Repo_Emp.Update(user);
        }
    }
}
