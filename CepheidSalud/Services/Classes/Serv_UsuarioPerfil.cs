﻿using Models;
using Repo.Interfaces;
using Services.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Services.Classes
{
    public class Serv_UsuarioPerfil : IServ_UsuarioPerfil
    {
        private IRepo<UsuarioPerfil> Repo_Emp;

        public Serv_UsuarioPerfil(IRepo<UsuarioPerfil> Repo_Emp)
        {
            this.Repo_Emp = Repo_Emp;
        }

        public List<UsuarioPerfil> GetUsuarioPerfils()
        {
            return Repo_Emp.GetAll().ToList();
        }

        public UsuarioPerfil GetUsuarioPerfil(long id)
        {
            return Repo_Emp.Get(id);
        }

        public void InsertUsuarioPerfil(UsuarioPerfil user)
        {
            Repo_Emp.Insert(user);
        }
        public void UpdateUsuarioPerfil(UsuarioPerfil user)
        {
            Repo_Emp.Update(user);
        }
    }
}
