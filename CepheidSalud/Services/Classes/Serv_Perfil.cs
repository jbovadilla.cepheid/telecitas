﻿using Models;
using Repo.Interfaces;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Services.Classes
{
   public  class Serv_Perfil : IServ_Perfil
    {
        private IRepo<Perfil> Repo_Emp;

        public Serv_Perfil(IRepo<Perfil> Repo_Emp)
        {
            this.Repo_Emp = Repo_Emp;
        }

        public List<Perfil> GetPerfils()
        {
            return Repo_Emp.GetAll().ToList();
        }

        public Perfil GetPerfil(long id)
        {
            return Repo_Emp.Get(id);
        }

        public void InsertPerfil(Perfil user)
        {
            user.IPrfl = ObtenerPerfilXModuloXSistema(user.ISstma, user.IMdlo).Count() + 1;
            Repo_Emp.Insert(user);
        }
        public void UpdatePerfil(Perfil user)
        {
            Repo_Emp.Update(user);
        }

        public List<Perfil> ObtenerPerfilXModuloXSistema(decimal id, decimal idM)
        {
            return GetPerfils().Where(x => x.ISstma == id && x.IMdlo == idM).ToList();
        }
    }
}
