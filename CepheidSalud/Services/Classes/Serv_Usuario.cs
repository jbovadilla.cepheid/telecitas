﻿using Models;
using Repo.Interfaces;
using Services.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Services.Classes
{
    public class Serv_Usuario : IServ_Usuario
    {
        private IRepo<Usuario> Repo_Usu;
        private IRepoEmpresa<Empresa> Repo_Emp;

        public Serv_Usuario(IRepo<Usuario> Repo_Usu, IRepoEmpresa<Empresa> Repo_Emp)
        {
            this.Repo_Usu = Repo_Usu;
            this.Repo_Emp = Repo_Emp;
        }

        public List<Usuario> GetUsuarios()
        {
            return Repo_Usu.GetAll().ToList();
        }

        public Usuario GetUsuario(long id)
        {
            return Repo_Usu.Get(id);
        }

        public void InsertUsuario(Usuario user)
        {
            Repo_Emp.Insert(new Empresa() { Nmbre = "Cepheid", Dscrpcn = "neceitabamos una empresa, y la creamos", UActlzcn = "CEPHEID", UCrcn = "CEPHEID" });
            Repo_Usu.Insert(user);
        }
        public void UpdateUsuario(Usuario user)
        {
            Repo_Usu.Update(user);
        }
    }
}
