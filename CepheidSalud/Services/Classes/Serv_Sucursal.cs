﻿using Microsoft.EntityFrameworkCore;
using Models;
using Repo.Interfaces;
using Services.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Services.Classes
{
    public class Serv_Sucursal : IServ_Sucursal
    {
        private IRepo<Sucursal> Repo_Suc;
        private IRepo<Empresa> Repo_Emp;

        public Serv_Sucursal(IRepo<Sucursal> Repo_Suc)
        {
            this.Repo_Suc = Repo_Suc;
        }
        public Serv_Sucursal(IRepo<Sucursal> Repo_Suc, IRepo<Empresa> Repo_Emp)
        {
            this.Repo_Suc = Repo_Suc;
            this.Repo_Emp = Repo_Emp;
        }

        public List<Sucursal> GetSucursales()
        {
            return Repo_Suc.GetAll().ToList();
        }
        public List<Sucursal> ObtenerSucursalxEmpresa(decimal idEmp)
        {
            return GetSucursales().Where(x => x.IEmprsa == idEmp).ToList();
        }

        public Sucursal GetSucursal(decimal id)
        {
            return Repo_Suc.Get(id);
        }

        public void InsertSucursal(Sucursal suc)
        {
            suc.IScrsl = ObtenerSucursalxEmpresa(suc.IEmprsa).Count() + 1;
            Repo_Suc.Insert(suc);
        }
        public void UpdateSucursal(Sucursal user)
        {
            Repo_Suc.Update(user);
        }
    }
}
