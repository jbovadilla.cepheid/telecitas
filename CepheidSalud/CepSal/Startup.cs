﻿using CepSal.Components;
using CepSal.Identity;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Models;
using Repo.Classes;
using Repo.Interfaces;
using Services.Classes;
using Services.Interfaces;
using System;
using System.Linq;
using System.Security.Claims;

namespace CepSal
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for  non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddSession();

            services.Configure<SecurityStampValidatorOptions>(options => options.ValidationInterval = TimeSpan.FromSeconds(10));
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options =>
                {
                    options.LoginPath = "/Account/Login/";
                    options.AccessDeniedPath = "/Account/Denegado/";
                    options.SlidingExpiration = true;
                    options.ExpireTimeSpan = TimeSpan.FromMinutes(30);
                });
            //****************************************************************************

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddDbContext<Repo.AppContext>
                (options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            //***************************************************************************
            services.AddAuthorization(options =>
            {
                options.AddPolicy("SuperUsuario", p =>
                {
                    p.RequireAssertion(context =>
                    {
                        return context.User.Claims
                              .Any(c => c.Type == ClaimTypes.Role && c.Value.Equals("Permitido"));
                    });
                });
                options.AddPolicy("TienePermisosEmpresa", policy => policy.Requirements.Add(new TienePermisosRequirement("Empresa")));
                options.AddPolicy("TienePermisosSucursal", policy => policy.Requirements.Add(new TienePermisosRequirement("Sucursal")));
                options.AddPolicy("TienePermisosSistema", policy => policy.Requirements.Add(new TienePermisosRequirement("Sistema")));
                options.AddPolicy("TienePermisosModulo", policy => policy.Requirements.Add(new TienePermisosRequirement("Modulo")));
                options.AddPolicy("TienePermisosOpcion", policy => policy.Requirements.Add(new TienePermisosRequirement("Opcion")));
                options.AddPolicy("TienePermisosOpcionDetalle", policy => policy.Requirements.Add(new TienePermisosRequirement("OpcionDetalle")));
                options.AddPolicy("TienePermisosPerfil", policy => policy.Requirements.Add(new TienePermisosRequirement("Perfil")));
                //options.AddPolicy("TienePermisosSucursal", policy => policy.Requirements.Add(new TienePermisosRequirement("Sucursal")));
                //options.AddPolicy("TienePermisosSucursal", policy => policy.Requirements.Add(new TienePermisosRequirement("Sucursal")));
                //options.AddPolicy("TienePermisosSucursal", policy => policy.Requirements.Add(new TienePermisosRequirement("Sucursal")));
                //options.AddPolicy("TienePermisosSucursal", policy => policy.Requirements.Add(new TienePermisosRequirement("Sucursal")));
                //options.AddPolicy("TienePermisosSucursal", policy => policy.Requirements.Add(new TienePermisosRequirement("Sucursal")));
                //options.AddPolicy("TienePermisosSucursal", policy => policy.Requirements.Add(new TienePermisosRequirement("Sucursal")));
                //options.AddPolicy("TienePermisosSucursal", policy => policy.Requirements.Add(new TienePermisosRequirement("Sucursal")));
                //options.AddPolicy("TienePermisosSucursal", policy => policy.Requirements.Add(new TienePermisosRequirement("Sucursal")));
                //options.AddPolicy("TienePermisosSucursal", policy => policy.Requirements.Add(new TienePermisosRequirement("Sucursal")));
                //options.AddPolicy("TienePermisosSucursal", policy => policy.Requirements.Add(new TienePermisosRequirement("Sucursal")));
            });

            //***************************************************************************
            //services.AddIdentity<Usuario, UsuarioPerfil>()
            //    .AddDefaultTokenProviders();
            services.AddTransient<IAuthorizationHandler, TienePermisosHandler>();
            services.AddTransient<IAppUserStore, AppUserStore>();
            services.AddTransient<IAppRoleStore, RoleStore>();
            //***************************************************************************

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();
            services.AddScoped<IViewRenderService, ViewRenderService>();
            services.AddScoped(typeof(IRepo<>), typeof(Repository<>));
            services.AddScoped(typeof(IRepoSistema<Sistema>), typeof(RepoSistema));
            services.AddScoped(typeof(IRepoEmpresa<Empresa>), typeof(RepoEmpresa));
            //services.AddScoped(typeof(IRepoErrPersona<ErrorPersona>), typeof(RepoErrPersona));
            services.AddScoped(typeof(IRepoErrSeguridad<ErrorSeguridad>), typeof(RepoErrSeguridad));
            //***************************************************************************
            services.AddTransient<IServ_Empresa, Serv_Empresa>();
            services.AddTransient<IServ_Sucursal, Serv_Sucursal>();
            services.AddTransient<IServ_Usuario, Serv_Usuario>();
            services.AddTransient<IServ_UsuarioPerfil, Serv_UsuarioPerfil>();
            services.AddTransient<IServ_Perfil, Serv_Perfil>();
            services.AddTransient<IServ_Sistema, Serv_Sistema>();
            services.AddTransient<IServ_Modulo, Serv_Modulo>();
            services.AddTransient<IServ_Opcion, Serv_Opcion>();
            services.AddTransient<IServ_OpcionPerfil, Serv_Opcion>();
            services.AddTransient<IServ_OpcionPerfilDetalle, Serv_Opcion>();
            services.AddTransient<IServ_OpcionDetalle, Serv_OpcionDetalle>();
            //***************************************************************************
        }
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }
            app.UseAuthentication();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseSession();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                        name: "default",
                        template: "{controller=Home}/{action=Index}/{id?}");
            });

        }
    }
}
