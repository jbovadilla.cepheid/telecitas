﻿namespace CepSal.ViewModels
{
    public class vmAjaxRequest
    {
        public int Status { get; set; }
        public string Message { get; set; }
        public string AjaxReturn { get; set; }
    }
}
