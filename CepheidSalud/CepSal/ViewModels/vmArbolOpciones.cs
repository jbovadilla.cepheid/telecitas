﻿using System.Collections.Generic;

namespace CepSal.ViewModels
{
    public class vmArbolOpciones
    {
        public int idSistema { get; set; }
        public int idModulo { get; set; }
        public int idPerfil { get; set; }
        public List<Nodos> nodos { get; set; }
    }
    public class Nodos
    {
        public string id { get; set; }
        public string text { get; set; }
        public state state { get; set; }
        public List<Nodos> nodes { get; set; }
    }
    public class state
    {
        public bool ischecked { get; set; }
    }
}
