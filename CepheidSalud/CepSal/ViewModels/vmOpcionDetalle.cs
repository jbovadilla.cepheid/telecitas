﻿using Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CepSal.ViewModels
{
    public class vmOpcionDetalle : vmAuditoria
    {
        public static implicit operator vmOpcionDetalle(OpcionDetalle u)
        {
            return new vmOpcionDetalle()
            {
                nId = u.nId,
                ISstma = Convert.ToInt32(u.ISstma),
                IMdlo = Convert.ToInt32(u.IMdlo),
                IOpcn = Convert.ToInt32(u.IOpcn),
                IODtlle = Convert.ToInt32(u.IODtlle),
                Nmbre = u.Nmbre,
                CEstdo = u.CEstdo,
                FEstdo = u.FEstdo,
                FActlzcn = u.FActlzcn,
                UActlzcn = u.UActlzcn,
                UCrcn = u.UCrcn,
                FCrcn = u.FCrcn
            };
        }

        public static implicit operator OpcionDetalle(vmOpcionDetalle u)
        {
            return new OpcionDetalle()
            {
                nId = u.nId,
                ISstma = u.ISstma,
                IMdlo = u.IMdlo,
                IOpcn = u.IOpcn,
                IODtlle = u.IODtlle,
                Nmbre = u.Nmbre,
                CEstdo = u.CEstdo,
                FEstdo = u.FEstdo,
                FActlzcn = u.FActlzcn,
                UActlzcn = u.UActlzcn,
                UCrcn = u.UCrcn,
                FCrcn = u.FCrcn
            };
        }
        public vmOpcionDetalle() : base()
        {
        }
        public vmOpcionDetalle(int id, int idM, int idOpDet) : base()
        {
            ISstma = id;
            IMdlo = idM;
            IOpcn = idOpDet;
        }

        [Display(Name = "Id")]
        [Required(ErrorMessage = "Seleccione una Opción Detalle.")]
        public int nId { get; set; }

        [Display(Name = "Sistema")]
        [Required(ErrorMessage = "Seleccione un Sistema.")]
        public int ISstma { get; set; }

        [Display(Name = "Modulo")]
        [Required(ErrorMessage = "Seleccione un Módulo.")]
        public decimal IMdlo { get; set; }

        public decimal IOpcn { get; set; }
        public decimal IODtlle { get; set; }

        public string Nmbre { get; set; }
    }

    public class vmBuscarOpcionesDetalle
    {
        public int idsistema { get; set; }
        public int idModulo { get; set; }
        public int idOpcion { get; set; }
    }
    public class vmListadoOpcionesDetalle
    {
        public int idsistema { get; set; }
        public int idModulo { get; set; }
        public int idOpcion { get; set; }
        public List<vmOpcionDetalle> OpcionsDetalles { get; set; }
    }
}
