﻿using Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CepSal.ViewModels
{
    public class vmSistema : vmAuditoria
    {
        public static implicit operator vmSistema(Sistema u)
        {
            return new vmSistema()
            {
                ISstma = Convert.ToInt32(u.ISstma),
                Nmbre = u.Nmbre,
                Dscrpcn = u.Dscrpcn,
                CEstdo = u.CEstdo,
                FEstdo = u.FEstdo,
                FActlzcn = u.FActlzcn,
                UActlzcn = u.UActlzcn,
                UCrcn = u.UCrcn,
                FCrcn = u.FCrcn
            };
        }

        public static implicit operator Sistema(vmSistema u)
        {
            return new Sistema
            {
                ISstma = u.ISstma,
                Nmbre = u.Nmbre,
                Dscrpcn = u.Dscrpcn,
                CEstdo = u.CEstdo,
                FEstdo = u.FEstdo,
                FActlzcn = u.FActlzcn,
                UActlzcn = u.UActlzcn,
                UCrcn = u.UCrcn,
                FCrcn = u.FCrcn
            };
        }
        public vmSistema() : base()
        {
        }
        /// <summary>
        /// sistema
        /// </summary>
        [Display(Name = "Id Sistema")]
        public int ISstma { get; set; }
        /// <summary>
        /// nombre
        /// </summary>
        [Display(Name ="Nombre Sistema")]
        [Required(ErrorMessage = "Por favor indique un nombre para el sistema.")]
        public string Nmbre { get; set; }
        /// <summary>
        /// descripcion
        /// </summary>
        [Display (Name ="Descripción")]
        [Required (ErrorMessage = "Por favor indique una desripción para el sistema.")]
        public string Dscrpcn { get; set; }

        public bool TieneModulo { get; set; }
    }
}
