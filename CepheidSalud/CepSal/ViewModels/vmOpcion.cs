﻿using Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CepSal.ViewModels
{
    public class vmOpcion : vmAuditoria
    {
        public static implicit operator vmOpcion(Opcion u)
        {
            return new vmOpcion()
            {
                nId = u.nId,
                ISstma = Convert.ToInt32(u.ISstma),
                IMdlo = Convert.ToInt32(u.IMdlo),
                IOpcn = Convert.ToInt32(u.IOpcn),
                IOpcnPdre = Convert.ToInt32(u.IOPdre),
                URL = u.URL,
                Nmbre = u.Nmbre,
                Dscrpcn = u.Dscrpcn,
                CEstdo = u.CEstdo,
                FEstdo = u.FEstdo,
                FActlzcn = u.FActlzcn,
                UActlzcn = u.UActlzcn,
                UCrcn = u.UCrcn,
                FCrcn = u.FCrcn
            };
        }
        public static implicit operator Opcion(vmOpcion u)
        {
            return new Opcion
            {
                nId = u.nId,
                ISstma = u.ISstma,
                IMdlo = u.IMdlo,
                IOpcn = u.IOpcn,
                URL = u.URL,
                IOPdre = u.IOpcnPdre,
                Nmbre = u.Nmbre,
                Dscrpcn = u.Dscrpcn,
                CEstdo = u.CEstdo,
                FEstdo = u.FEstdo,
                FActlzcn = u.FActlzcn,
                UActlzcn = u.UActlzcn,
                UCrcn = u.UCrcn,
                FCrcn = u.FCrcn
            };
        }
        public vmOpcion() : base()
        {
        }
        public vmOpcion(int id, int idM) : base()
        {
            ISstma = id;
            IMdlo = idM;
        }

        [Display(Name = "Id")]
        [Required(ErrorMessage = "Seleccione una Opción.")]
        public int nId { get; set; }

        [Display(Name = "Sistema")]
        [Required(ErrorMessage = "Seleccione un Sistema.")]
        public int ISstma { get; set; }

        [Display(Name = "Modulo")]
        [Required(ErrorMessage = "Seleccione un Módulo.")]
        public decimal IMdlo { get; set; }
        public string URL { get; set; }
        public decimal IOpcn { get; set; }
        [Display(Name = "Pertenece a")]
        public decimal? IOpcnPdre { get; set; }
        public string NombrePadre { get; set; }
        [Display(Name = "Nombre Opción")]
        [Required(ErrorMessage = "Por favor indique un nombre para la Opción.")]
        public string Nmbre { get; set; }
        [Display(Name = "Descripción")]
        [Required(ErrorMessage = "Por favor indique una desripción para la Opción.")]
        public string Dscrpcn { get; set; }

        public List<vmOpcion> OpcionPadre { get; set; }
    }

    public class vmBuscarOpciones
    {
        public int idsistema { get; set; }
        public int idModulo { get; set; }
    }
    public class vmListadoOpciones
    {
        public int idsistema { get; set; }
        public int idModulo { get; set; }
        public List<vmOpcion> Opcions { get; set; }
    }
}
