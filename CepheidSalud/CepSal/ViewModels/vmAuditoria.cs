﻿using Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CepSal.ViewModels
{
    public class vmAuditoria
    {
        /*public vmAuditoria()
        {
        }*/
        public vmAuditoria()
        {
            CEstdo = Codigos.Estado.Activo;
            FEstdo = DateTime.Now;
            FActlzcn = DateTime.Now;
            //UActlzcn = usuario;
            FCrcn = DateTime.Now;
            //UCrcn = usuario;
        }
        /// <summary>
        /// Cambio Estado
        /// </summary>
        [Display(Name = "Estado")]
        [Required(ErrorMessage = "Seleccione un estado.")]
        public string CEstdo { get; set; }
        /// <summary>
        /// Fecha cambio Estado
        /// </summary>
        [Display(Name = "Fecha")]
        [Required(ErrorMessage = "Por favor indique una fecha.")]
        public DateTime FEstdo { get; set; }
        /// <summary>
        /// Fecha actualizacion
        /// </summary>
        [Display(Name = "Fecha Última Actualización")]
        [Required(ErrorMessage = "Por favor indique una fecha.")]
        public DateTime FActlzcn { get; set; }
        /// <summary>
        /// Usuario actualizacion
        /// </summary>
        [Display(Name = "Usuario Actualización")]
        [Required(ErrorMessage = "Por favor indique un usuario.")]
        public string UActlzcn { get; set; }
        /// <summary>
        /// Fecha de Creacion
        /// </summary>
        [Display(Name = "Fecha Creación")]
        [Required(ErrorMessage = "Por favor indique una fecha.")]
        public DateTime FCrcn { get; set; }
        /// <summary>
        /// Usuario creador
        /// </summary>
        [Display(Name = "Usuario Creador")]
        [Required(ErrorMessage = "Por favor indique un usuario.")]
        public string UCrcn { get; set; }

        public List<OptionItems> Estados
        {
            get
            {
                var a = new List<OptionItems>
                {
                    new OptionItems {value = "A", name = "Activo"},
                    new OptionItems {value = "I", name = "Inactivo"},
                };

                return a;
            }
        }
    }
}
