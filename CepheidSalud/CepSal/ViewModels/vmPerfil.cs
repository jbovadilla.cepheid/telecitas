﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Models;

namespace CepSal.ViewModels
{
    public class vmPerfil : vmAuditoria
    {
        public static implicit operator vmPerfil(Perfil u)
        {
            return new vmPerfil()
            {
                nId = u.nId,
                ISstma = Convert.ToInt32(u.ISstma),
                IMdlo = Convert.ToInt32(u.IMdlo),
                IPrfl = Convert.ToInt32(u.IPrfl),
                Nmbre = u.Nmbre,
                Dscrpcn = u.Dscrpcn,
                CEstdo = u.CEstdo,
                FEstdo = u.FEstdo,
                FActlzcn = u.FActlzcn,
                UActlzcn = u.UActlzcn,
                UCrcn = u.UCrcn,
                FCrcn = u.FCrcn
            };
        }
        public static implicit operator Perfil(vmPerfil u)
        {
            return new Perfil
            {
                nId = u.nId,
                ISstma = u.ISstma,
                IMdlo = u.IMdlo,
                IPrfl = u.IPrfl,
                Nmbre = u.Nmbre,
                Dscrpcn = u.Dscrpcn,
                CEstdo = u.CEstdo,
                FEstdo = u.FEstdo,
                FActlzcn = u.FActlzcn,
                UActlzcn = u.UActlzcn,
                UCrcn = u.UCrcn,
                FCrcn = u.FCrcn
            };
        }
        public vmPerfil() : base()
        {
        }
        public vmPerfil(int id,int idM) : base()
        {
            ISstma = id;
            IMdlo = idM;
        }

        [Display(Name = "Id")]
        [Required(ErrorMessage = "Seleccione un Perfil.")]
        public int nId { get; set; }

        [Display(Name = "Modulo")]
        [Required(ErrorMessage = "Seleccione un Módulo.")]
        public int IMdlo { get; set; }

        [Display(Name = "Sistema")]
        [Required(ErrorMessage = "Seleccione un Sistema.")]
        public int ISstma { get; set; }

        public decimal IPrfl { get; set; }
        [Display(Name = "Nombre Perfil")]
        [Required(ErrorMessage = "Por favor indique un nombre para el perfil.")]
        public string Nmbre { get; set; }
        [Display(Name = "Descripción")]
        [Required(ErrorMessage = "Por favor indique una desripción para el perfil.")]
        public string Dscrpcn { get; set; }

    }

    public class vmBuscarPerfiles
    {
        public int idsistema { get; set; }
        public int idmodulo { get; set; }
    }
    public class vmListadoPerfiles
    {
        public int idsistema { get; set; }
        public int idmodulo { get; set; }
        public List<vmPerfil> Perfiles { get; set; }
    }
}
