﻿using Models;
using System;
using System.ComponentModel.DataAnnotations;

namespace CepSal.ViewModels
{
    public class vmEmpresa : vmAuditoria
    {
        public static implicit operator vmEmpresa(Empresa u)
        {
            return new vmEmpresa()
            {
                IEmprsa = Convert.ToInt32(u.IEmprsa),
                Nmbre = u.Nmbre,
                Dscrpcn = u.Dscrpcn,
                CEstdo = u.CEstdo,
                FEstdo = u.FEstdo,
                FActlzcn = u.FActlzcn,
                UActlzcn = u.UActlzcn,
                UCrcn = u.UCrcn,
                FCrcn = u.FCrcn
            };
        }
        public static implicit operator Empresa(vmEmpresa u)
        {
            return new Empresa()
            {
                nId = u.IEmprsa,
                IEmprsa = u.IEmprsa,
                Nmbre = u.Nmbre,
                Dscrpcn = u.Dscrpcn,
                CEstdo = u.CEstdo,
                FEstdo = u.FEstdo,
                FActlzcn = u.FActlzcn,
                UActlzcn = u.UActlzcn,
                UCrcn = u.UCrcn,
                FCrcn = u.FCrcn
            };
        }
        public vmEmpresa() : base()
        {
        }
        /// <summary>
        /// empresa
        /// </summary>
        [Display(Name = "Id Empresa")]
        public int IEmprsa { get; set; }
        /// <summary>
        /// nombre
        /// </summary>
        [Display(Name = "Nombre Empresa")]
        [Required(ErrorMessage = "Por favor indique un nombre para la empresa.")]
        public string Nmbre { get; set; }
        /// <summary>
        /// descripcion
        /// </summary>
        [Display(Name = "Descripción")]
        [Required(ErrorMessage = "Por favor indique una desripción para la empresa.")]
        public string Dscrpcn { get; set; }
        public bool TieneSucursales { get; set; }

    }
}
