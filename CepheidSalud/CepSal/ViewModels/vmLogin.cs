﻿using System.ComponentModel.DataAnnotations;

namespace CepSal.ViewModels
{
    public class vmLogin
    {
        [Required(ErrorMessage = "Usuario requerido.")]
        public string Usuario { get; set; }
        [Required(ErrorMessage = "Empresa requerida.")]
        public int idEmpresa { get; set; }
        [Required(ErrorMessage = "Sucursal requerida.")]
        public int IdSucursal { get; set; }
        [Required(ErrorMessage = "Contraseña requerida.")]
        public string Contrasena { get; set; }
    }
}
