#pragma checksum "C:\Users\Computer\Source\Workspaces\TFS3\CepheidSalud\CepheidSalud\CepSal\Views\Opcion\_ListadoOpciones.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "7e2dbdca25fdfdaad7f2379d97201de4c2c372f1"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Opcion__ListadoOpciones), @"mvc.1.0.view", @"/Views/Opcion/_ListadoOpciones.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Opcion/_ListadoOpciones.cshtml", typeof(AspNetCore.Views_Opcion__ListadoOpciones))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\Computer\Source\Workspaces\TFS3\CepheidSalud\CepheidSalud\CepSal\Views\_ViewImports.cshtml"
using CepSal;

#line default
#line hidden
#line 2 "C:\Users\Computer\Source\Workspaces\TFS3\CepheidSalud\CepheidSalud\CepSal\Views\_ViewImports.cshtml"
using Models;

#line default
#line hidden
#line 3 "C:\Users\Computer\Source\Workspaces\TFS3\CepheidSalud\CepheidSalud\CepSal\Views\_ViewImports.cshtml"
using CepSal.ViewModels;

#line default
#line hidden
#line 6 "C:\Users\Computer\Source\Workspaces\TFS3\CepheidSalud\CepheidSalud\CepSal\Views\_ViewImports.cshtml"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#line 8 "C:\Users\Computer\Source\Workspaces\TFS3\CepheidSalud\CepheidSalud\CepSal\Views\_ViewImports.cshtml"
using Microsoft.AspNetCore.Http;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"7e2dbdca25fdfdaad7f2379d97201de4c2c372f1", @"/Views/Opcion/_ListadoOpciones.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"d9a8162adbd3a173fa8bb3c954f6988378f56429", @"/Views/_ViewImports.cshtml")]
    public class Views_Opcion__ListadoOpciones : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<vmListadoOpciones>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(26, 167, true);
            WriteLiteral("\r\n<a id=\"edit\"\r\n   data-toggle=\"modal\"\r\n   data-target=\"#modal-action\"\r\n   data-ajax-success=\"success\"\r\n   data-ajax=\"true\"\r\n   data-ajax-url=\"Opcion/AgregarOpcion?id=");
            EndContext();
            BeginContext(194, 15, false);
#line 8 "C:\Users\Computer\Source\Workspaces\TFS3\CepheidSalud\CepheidSalud\CepSal\Views\Opcion\_ListadoOpciones.cshtml"
                                     Write(Model.idsistema);

#line default
#line hidden
            EndContext();
            BeginContext(209, 5, true);
            WriteLiteral("&idM=");
            EndContext();
            BeginContext(215, 14, false);
#line 8 "C:\Users\Computer\Source\Workspaces\TFS3\CepheidSalud\CepheidSalud\CepSal\Views\Opcion\_ListadoOpciones.cshtml"
                                                          Write(Model.idModulo);

#line default
#line hidden
            EndContext();
            BeginContext(229, 1, true);
            WriteLiteral("\"");
            EndContext();
            BeginWriteAttribute("class", "\r\n   class=\"", 230, "\"", 263, 1);
#line 9 "C:\Users\Computer\Source\Workspaces\TFS3\CepheidSalud\CepheidSalud\CepSal\Views\Opcion\_ListadoOpciones.cshtml"
WriteAttributeValue("", 242, ColorBoton.Principal, 242, 21, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(264, 10, true);
            WriteLiteral(">\r\n    <em");
            EndContext();
            BeginWriteAttribute("class", " class=\"", 274, "\"", 301, 1);
#line 10 "C:\Users\Computer\Source\Workspaces\TFS3\CepheidSalud\CepheidSalud\CepSal\Views\Opcion\_ListadoOpciones.cshtml"
WriteAttributeValue("", 282, IconoBoton.Agregar, 282, 19, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(302, 47, true);
            WriteLiteral("></em>  Agregar\r\n</a>\r\n<table id=\"tablaOpcion2\"");
            EndContext();
            BeginWriteAttribute("class", " class=\"", 349, "\"", 382, 2);
#line 12 "C:\Users\Computer\Source\Workspaces\TFS3\CepheidSalud\CepheidSalud\CepSal\Views\Opcion\_ListadoOpciones.cshtml"
WriteAttributeValue("", 357, Tabla.EstiloTabla, 357, 18, false);

#line default
#line hidden
            WriteAttributeValue("  ", 375, "panel", 377, 7, true);
            EndWriteAttribute();
            BeginContext(383, 305, true);
            WriteLiteral(@">
    <thead>
        <tr>
            <th>Id</th>
            <th>Nombre</th>
            <th>Descripción</th>
            <th>Pertenece a</th>
            <th>URL</th>
            <th>Fecha Act.</th>
            <th>Estado</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
");
            EndContext();
#line 26 "C:\Users\Computer\Source\Workspaces\TFS3\CepheidSalud\CepheidSalud\CepSal\Views\Opcion\_ListadoOpciones.cshtml"
         if (Model != null)
        {
            

#line default
#line hidden
#line 28 "C:\Users\Computer\Source\Workspaces\TFS3\CepheidSalud\CepheidSalud\CepSal\Views\Opcion\_ListadoOpciones.cshtml"
             foreach (var item in Model.Opcions)
            {

#line default
#line hidden
            BeginContext(793, 46, true);
            WriteLiteral("                <tr>\r\n                    <td>");
            EndContext();
            BeginContext(840, 40, false);
#line 31 "C:\Users\Computer\Source\Workspaces\TFS3\CepheidSalud\CepheidSalud\CepSal\Views\Opcion\_ListadoOpciones.cshtml"
                   Write(Html.DisplayFor(modelItem => item.IOpcn));

#line default
#line hidden
            EndContext();
            BeginContext(880, 31, true);
            WriteLiteral("</td>\r\n                    <td>");
            EndContext();
            BeginContext(912, 40, false);
#line 32 "C:\Users\Computer\Source\Workspaces\TFS3\CepheidSalud\CepheidSalud\CepSal\Views\Opcion\_ListadoOpciones.cshtml"
                   Write(Html.DisplayFor(modelItem => item.Nmbre));

#line default
#line hidden
            EndContext();
            BeginContext(952, 31, true);
            WriteLiteral("</td>\r\n                    <td>");
            EndContext();
            BeginContext(984, 42, false);
#line 33 "C:\Users\Computer\Source\Workspaces\TFS3\CepheidSalud\CepheidSalud\CepSal\Views\Opcion\_ListadoOpciones.cshtml"
                   Write(Html.DisplayFor(modelItem => item.Dscrpcn));

#line default
#line hidden
            EndContext();
            BeginContext(1026, 31, true);
            WriteLiteral("</td>\r\n                    <td>");
            EndContext();
            BeginContext(1058, 46, false);
#line 34 "C:\Users\Computer\Source\Workspaces\TFS3\CepheidSalud\CepheidSalud\CepSal\Views\Opcion\_ListadoOpciones.cshtml"
                   Write(Html.DisplayFor(modelItem => item.NombrePadre));

#line default
#line hidden
            EndContext();
            BeginContext(1104, 31, true);
            WriteLiteral("</td>\r\n                    <td>");
            EndContext();
            BeginContext(1136, 38, false);
#line 35 "C:\Users\Computer\Source\Workspaces\TFS3\CepheidSalud\CepheidSalud\CepSal\Views\Opcion\_ListadoOpciones.cshtml"
                   Write(Html.DisplayFor(modelItem => item.URL));

#line default
#line hidden
            EndContext();
            BeginContext(1174, 31, true);
            WriteLiteral("</td>\r\n                    <td>");
            EndContext();
            BeginContext(1206, 43, false);
#line 36 "C:\Users\Computer\Source\Workspaces\TFS3\CepheidSalud\CepheidSalud\CepSal\Views\Opcion\_ListadoOpciones.cshtml"
                   Write(Html.DisplayFor(modelItem => item.FActlzcn));

#line default
#line hidden
            EndContext();
            BeginContext(1249, 33, true);
            WriteLiteral("</td>\r\n                    <td>\r\n");
            EndContext();
#line 38 "C:\Users\Computer\Source\Workspaces\TFS3\CepheidSalud\CepheidSalud\CepSal\Views\Opcion\_ListadoOpciones.cshtml"
                         if (item.CEstdo == "A")
                        {

#line default
#line hidden
            BeginContext(1359, 77, true);
            WriteLiteral("                            <span class=\"label label-success\">Activo</span>\r\n");
            EndContext();
#line 41 "C:\Users\Computer\Source\Workspaces\TFS3\CepheidSalud\CepheidSalud\CepSal\Views\Opcion\_ListadoOpciones.cshtml"
                        }
                        else
                        {

#line default
#line hidden
            BeginContext(1520, 78, true);
            WriteLiteral("                            <span class=\"label label-danger\">Inactivo</span>\r\n");
            EndContext();
#line 45 "C:\Users\Computer\Source\Workspaces\TFS3\CepheidSalud\CepheidSalud\CepSal\Views\Opcion\_ListadoOpciones.cshtml"
                        }

#line default
#line hidden
            BeginContext(1625, 495, true);
            WriteLiteral(@"                    </td>

                    <td>
                        <span class=""tool-tip"" data-toggle=""tooltip"" data-placement=""top"" title=""Editar"">
                            <a id=""edit""
                               data-toggle=""modal""
                               data-target=""#modal-action""
                               data-ajax-success=""success""
                               data-ajax=""true""
                               data-ajax-url=""/Opcion/EditarOpcion?id=");
            EndContext();
            BeginContext(2121, 8, false);
#line 55 "C:\Users\Computer\Source\Workspaces\TFS3\CepheidSalud\CepheidSalud\CepSal\Views\Opcion\_ListadoOpciones.cshtml"
                                                                 Write(item.nId);

#line default
#line hidden
            EndContext();
            BeginContext(2129, 1, true);
            WriteLiteral("\"");
            EndContext();
            BeginWriteAttribute("class", "\r\n                               class=\"", 2130, "\"", 2186, 1);
#line 56 "C:\Users\Computer\Source\Workspaces\TFS3\CepheidSalud\CepheidSalud\CepSal\Views\Opcion\_ListadoOpciones.cshtml"
WriteAttributeValue("", 2170, ColorBoton.Info, 2170, 16, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(2187, 38, true);
            WriteLiteral(">\r\n                                <em");
            EndContext();
            BeginWriteAttribute("class", " class=\"", 2225, "\"", 2251, 1);
#line 57 "C:\Users\Computer\Source\Workspaces\TFS3\CepheidSalud\CepheidSalud\CepSal\Views\Opcion\_ListadoOpciones.cshtml"
WriteAttributeValue("", 2233, IconoBoton.Editar, 2233, 18, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(2252, 319, true);
            WriteLiteral(@"></em>
                            </a>
                        </span>
                        <span class=""tool-tip"" data-toggle=""tooltip"" data-placement=""top"" title=""Eliminar"">
                            <a id=""delete""
                               data-ajax-confirm=""Está seguro que desea eliminar la Opción ");
            EndContext();
            BeginContext(2572, 10, false);
#line 62 "C:\Users\Computer\Source\Workspaces\TFS3\CepheidSalud\CepheidSalud\CepSal\Views\Opcion\_ListadoOpciones.cshtml"
                                                                                      Write(item.Nmbre);

#line default
#line hidden
            EndContext();
            BeginContext(2582, 257, true);
            WriteLiteral(@" ?""
                               data-ajax-success=""successElimina""
                               data-ajax=""true""
                               data-ajax-update=""#tablaOpcion""
                               data-ajax-url=""/Opcion/EliminarOpcion?id=");
            EndContext();
            BeginContext(2840, 8, false);
#line 66 "C:\Users\Computer\Source\Workspaces\TFS3\CepheidSalud\CepheidSalud\CepSal\Views\Opcion\_ListadoOpciones.cshtml"
                                                                   Write(item.nId);

#line default
#line hidden
            EndContext();
            BeginContext(2848, 1, true);
            WriteLiteral("\"");
            EndContext();
            BeginWriteAttribute("class", "\r\n                               class=\"", 2849, "\"", 2906, 1);
#line 67 "C:\Users\Computer\Source\Workspaces\TFS3\CepheidSalud\CepheidSalud\CepSal\Views\Opcion\_ListadoOpciones.cshtml"
WriteAttributeValue("", 2889, ColorBoton.Error, 2889, 17, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(2907, 38, true);
            WriteLiteral(">\r\n                                <em");
            EndContext();
            BeginWriteAttribute("class", " class=\"", 2945, "\"", 2973, 1);
#line 68 "C:\Users\Computer\Source\Workspaces\TFS3\CepheidSalud\CepheidSalud\CepSal\Views\Opcion\_ListadoOpciones.cshtml"
WriteAttributeValue("", 2953, IconoBoton.Eliminar, 2953, 20, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(2974, 125, true);
            WriteLiteral("></em>\r\n                            </a>\r\n                        </span>\r\n                    </td>\r\n                </tr>\r\n");
            EndContext();
#line 73 "C:\Users\Computer\Source\Workspaces\TFS3\CepheidSalud\CepheidSalud\CepSal\Views\Opcion\_ListadoOpciones.cshtml"
            }

#line default
#line hidden
#line 73 "C:\Users\Computer\Source\Workspaces\TFS3\CepheidSalud\CepheidSalud\CepSal\Views\Opcion\_ListadoOpciones.cshtml"
             
        }

#line default
#line hidden
            BeginContext(3125, 26, true);
            WriteLiteral("    </tbody>\r\n</table>\r\n\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public IAuthorizationService AuthorizationService { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<vmListadoOpciones> Html { get; private set; }
    }
}
#pragma warning restore 1591
