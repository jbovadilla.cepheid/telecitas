#pragma checksum "C:\Users\Computer\Source\Workspaces\TFS3\CepheidSalud\CepheidSalud\CepSal\Views\Sistema\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "98326f6c8f375b339625166d39b05504ae39060f"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Sistema_Index), @"mvc.1.0.view", @"/Views/Sistema/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Sistema/Index.cshtml", typeof(AspNetCore.Views_Sistema_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\Computer\Source\Workspaces\TFS3\CepheidSalud\CepheidSalud\CepSal\Views\_ViewImports.cshtml"
using CepSal;

#line default
#line hidden
#line 2 "C:\Users\Computer\Source\Workspaces\TFS3\CepheidSalud\CepheidSalud\CepSal\Views\_ViewImports.cshtml"
using Models;

#line default
#line hidden
#line 3 "C:\Users\Computer\Source\Workspaces\TFS3\CepheidSalud\CepheidSalud\CepSal\Views\_ViewImports.cshtml"
using CepSal.ViewModels;

#line default
#line hidden
#line 6 "C:\Users\Computer\Source\Workspaces\TFS3\CepheidSalud\CepheidSalud\CepSal\Views\_ViewImports.cshtml"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#line 8 "C:\Users\Computer\Source\Workspaces\TFS3\CepheidSalud\CepheidSalud\CepSal\Views\_ViewImports.cshtml"
using Microsoft.AspNetCore.Http;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"98326f6c8f375b339625166d39b05504ae39060f", @"/Views/Sistema/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"d9a8162adbd3a173fa8bb3c954f6988378f56429", @"/Views/_ViewImports.cshtml")]
    public class Views_Sistema_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<List<vmSistema>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", "~/js/Sistema.js", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.ScriptTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_ScriptTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 2 "C:\Users\Computer\Source\Workspaces\TFS3\CepheidSalud\CepheidSalud\CepSal\Views\Sistema\Index.cshtml"
  
        ViewData["Title"] = "Sistemas";

#line default
#line hidden
            BeginContext(72, 166, true);
            WriteLiteral("\r\n<a id=\"edit\"\r\n   data-toggle=\"modal\"\r\n   data-target=\"#modal-action\"\r\n   data-ajax-success=\"success\"\r\n   data-ajax=\"true\"\r\n   data-ajax-url=\"Sistema/AgregarSistema\"");
            EndContext();
            BeginWriteAttribute("class", "\r\n   class=\"", 238, "\"", 271, 1);
#line 12 "C:\Users\Computer\Source\Workspaces\TFS3\CepheidSalud\CepheidSalud\CepSal\Views\Sistema\Index.cshtml"
WriteAttributeValue("", 250, ColorBoton.Principal, 250, 21, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(272, 10, true);
            WriteLiteral(">\r\n    <em");
            EndContext();
            BeginWriteAttribute("class", " class=\"", 282, "\"", 309, 1);
#line 13 "C:\Users\Computer\Source\Workspaces\TFS3\CepheidSalud\CepheidSalud\CepSal\Views\Sistema\Index.cshtml"
WriteAttributeValue("", 290, IconoBoton.Agregar, 290, 19, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(310, 48, true);
            WriteLiteral("></em> Agregar\r\n</a>\r\n\r\n<table id=\"tablaSistema\"");
            EndContext();
            BeginWriteAttribute("class", " class=\"", 358, "\"", 390, 2);
#line 16 "C:\Users\Computer\Source\Workspaces\TFS3\CepheidSalud\CepheidSalud\CepSal\Views\Sistema\Index.cshtml"
WriteAttributeValue("", 366, Tabla.EstiloTabla, 366, 18, false);

#line default
#line hidden
            WriteAttributeValue(" ", 384, "panel", 385, 6, true);
            EndWriteAttribute();
            BeginContext(391, 7, true);
            WriteLiteral(">\r\n    ");
            EndContext();
            BeginContext(399, 50, false);
#line 17 "C:\Users\Computer\Source\Workspaces\TFS3\CepheidSalud\CepheidSalud\CepSal\Views\Sistema\Index.cshtml"
Write(await Html.PartialAsync("_ListadoSistemas", Model));

#line default
#line hidden
            EndContext();
            BeginContext(449, 14, true);
            WriteLiteral("\r\n</table>\r\n\r\n");
            EndContext();
            BeginContext(464, 138, false);
#line 20 "C:\Users\Computer\Source\Workspaces\TFS3\CepheidSalud\CepheidSalud\CepSal\Views\Sistema\Index.cshtml"
Write(await Html.PartialAsync("_Modal",new ModalModel { ID ="modal-action",AreaLabeledId="modal-action-label",Size=Codigos.TamanoModal.Mediana}));

#line default
#line hidden
            EndContext();
            BeginContext(602, 4, true);
            WriteLiteral("\r\n\r\n");
            EndContext();
            DefineSection("scripts", async() => {
                BeginContext(625, 6, true);
                WriteLiteral("\r\n    ");
                EndContext();
                BeginContext(631, 65, false);
                __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "98326f6c8f375b339625166d39b05504ae39060f7059", async() => {
                }
                );
                __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
                __Microsoft_AspNetCore_Mvc_TagHelpers_ScriptTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.ScriptTagHelper>();
                __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_ScriptTagHelper);
                __Microsoft_AspNetCore_Mvc_TagHelpers_ScriptTagHelper.Src = (string)__tagHelperAttribute_0.Value;
                __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
#line 24 "C:\Users\Computer\Source\Workspaces\TFS3\CepheidSalud\CepheidSalud\CepSal\Views\Sistema\Index.cshtml"
__Microsoft_AspNetCore_Mvc_TagHelpers_ScriptTagHelper.AppendVersion = true;

#line default
#line hidden
                __tagHelperExecutionContext.AddTagHelperAttribute("asp-append-version", __Microsoft_AspNetCore_Mvc_TagHelpers_ScriptTagHelper.AppendVersion, global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
                await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
                if (!__tagHelperExecutionContext.Output.IsContentModified)
                {
                    await __tagHelperExecutionContext.SetOutputContentAsync();
                }
                Write(__tagHelperExecutionContext.Output);
                __tagHelperExecutionContext = __tagHelperScopeManager.End();
                EndContext();
                BeginContext(696, 3, true);
                WriteLiteral(" \r\n");
                EndContext();
            }
            );
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public IAuthorizationService AuthorizationService { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<List<vmSistema>> Html { get; private set; }
    }
}
#pragma warning restore 1591
