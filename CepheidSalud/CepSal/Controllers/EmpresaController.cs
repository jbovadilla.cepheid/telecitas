﻿using CepSal.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Models;
using Services.Interfaces;
using System;
using System.Collections.Generic;

namespace CepSal.Controllers
{
    [Authorize(Policy = "TienePermisosEmpresa")]
    public class EmpresaController : Controller
    {
        private readonly IServ_Empresa EmpresaServ;
        private readonly IServ_Sucursal SucursalServ;
        public EmpresaController(IServ_Empresa EmpresaServ, IServ_Sucursal SucursalServ)
        {
            this.EmpresaServ = EmpresaServ;
            this.SucursalServ = SucursalServ;
        }
        public List<vmEmpresa> BuscarEmpresas()
        {
            var emp = new List<vmEmpresa>();
            EmpresaServ.GetEmpresas().ForEach(u =>
            {
                var a = SucursalServ.ObtenerSucursalxEmpresa(u.IEmprsa).Count;
                var vm = new vmEmpresa();
                vm.IEmprsa = Convert.ToInt32(u.IEmprsa);
                vm.Nmbre = u.Nmbre;
                vm.Dscrpcn = u.Dscrpcn;
                vm.CEstdo = u.CEstdo;
                vm.FEstdo = u.FEstdo;
                vm.FActlzcn = u.FActlzcn;
                vm.UActlzcn = u.UActlzcn;
                vm.UCrcn = u.UCrcn;
                vm.FCrcn = u.FCrcn;
                vm.TieneSucursales = (a > 0) ? true : false;
                emp.Add(vm);
            });
            return emp;
        }
        [HttpGet]
        public IActionResult Index()
        {
            List<vmEmpresa> model = BuscarEmpresas();
            return View(model);
        }
        [HttpGet]
        public IActionResult AgregarEmpresa()
        {
            vmEmpresa model = new vmEmpresa();
            model.UCrcn = HttpContext.User.Identity.Name;
            model.UActlzcn = HttpContext.User.Identity.Name;
            return PartialView("_AgregarEditarEmpresa", model);
        }
        public IActionResult EditarEmpresa(int? id)
        {
            vmEmpresa model = new vmEmpresa();
            if (id.HasValue)
            {
                Empresa emp = EmpresaServ.GetEmpresa(id.Value);
                if (emp != null)
                {
                    model = emp;
                    model.UActlzcn = HttpContext.User.Identity.Name;
                    model.FActlzcn = DateTime.Now;
                }
            }
            return PartialView("_AgregarEditarEmpresa", model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AgregarEditarEmpresa(vmEmpresa vmEmp)
        {
            try
            {
                Empresa emp = null;
                if (vmEmp.IEmprsa == 0)
                {
                    emp = vmEmp;
                    emp.UActlzcn = HttpContext.User.Identity.Name;
                    emp.UCrcn = HttpContext.User.Identity.Name;
                    EmpresaServ.InsertEmpresa(emp);
                }
                else
                {
                    emp = EmpresaServ.GetEmpresa(vmEmp.IEmprsa);
                    emp.IEmprsa = vmEmp.IEmprsa;
                    emp.Nmbre = vmEmp.Nmbre;
                    emp.Dscrpcn = vmEmp.Dscrpcn;
                    emp.CEstdo = vmEmp.CEstdo;
                    emp.FEstdo = vmEmp.FEstdo;
                    emp.FActlzcn = DateTime.Now;
                    emp.UActlzcn = HttpContext.User.Identity.Name;

                    EmpresaServ.UpdateEmpresa(emp);
                }
                List<vmEmpresa> model = BuscarEmpresas();
                return PartialView("_ListadoEmpresas", model);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { Mensaje = ex.Message });
            }
        }
        [HttpGet]
        public IActionResult EliminarEmpresa(int id)
        {
            try
            {
                Empresa emp = EmpresaServ.GetEmpresa(id);
                emp.CEstdo = "I";
                EmpresaServ.UpdateEmpresa(emp);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { Mensaje = ex.Message });
            }

            List<vmEmpresa> model = BuscarEmpresas();
            return PartialView("_ListadoEmpresas", model);
        }
    }
}