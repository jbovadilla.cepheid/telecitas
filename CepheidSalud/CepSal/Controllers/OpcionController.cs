﻿using CepSal.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Models;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CepSal.Controllers
{

    [Authorize(Policy = "TienePermisosOpcion")]
    public class OpcionController : Controller
    {
        private readonly IServ_Opcion OpcionServ;
        private readonly IServ_Modulo ModuloServ;
        private readonly IServ_Sistema SistemaServ;

        public OpcionController(IServ_Opcion OpcionServ, IServ_Modulo ModuloServ, IServ_Sistema SistemaServ)
        {
            this.OpcionServ = OpcionServ;
            this.ModuloServ = ModuloServ;
            this.SistemaServ = SistemaServ;
        }
        public List<vmSistema> Buscarsistema()
        {
            var sis = new List<vmSistema>();
            SistemaServ.GetSistemas().ForEach(u =>
            {
                sis.Add(u);
            });
            return sis;
        }
        public List<vmModulo> BuscarModulo()
        {
            var sis = new List<vmModulo>();
            ModuloServ.GetModulos().ForEach(u =>
            {
                sis.Add(u);
            });
            return sis;
        }
        public vmListadoOpciones BuscarOpcionXModuloXSistema(vmBuscarOpciones vm)
        {
            vmListadoOpciones lis = new vmListadoOpciones();
            lis.Opcions = new List<vmOpcion>();
            lis.idsistema = vm.idsistema;
            lis.idModulo = vm.idModulo;
            OpcionServ.ObtenerOpcionXModuloXSistema(vm.idsistema, vm.idModulo).ForEach(u =>
            {
                lis.Opcions.Add(u);
            });
            lis.Opcions.ForEach(u =>
            {
                if (u.IOpcnPdre.HasValue && Convert.ToDecimal(u.IOpcnPdre.Value) != 0)
                    u.NombrePadre = OpcionServ.ObtenerOpcionXModuloXSistema(vm.idsistema, vm.idModulo).FirstOrDefault(s => s.IOpcn == Convert.ToDecimal(u.IOpcnPdre.Value)).Nmbre;
            });        
            return lis;
        }

        public vmListadoModulos BuscarModuloXSistemas(int id)
        {
            vmListadoModulos lis = new vmListadoModulos();
            lis.Modulos = new List<vmModulo>();
            lis.idsistema = id;
            ModuloServ.ObtenerModuloXSistema(id).ForEach(u =>
            {
                lis.Modulos.Add(u);
            });
            return lis;
        }

        [HttpGet]
        public IActionResult Index()
        {
            ViewBag.Sistemas = Buscarsistema();
            vmBuscarOpciones vm = new vmBuscarOpciones { idsistema = 0, idModulo = 0 };
            return View(vm);
        }

        [HttpPost]
        public IActionResult BuscarOpcionesXModuloXSistema(vmBuscarOpciones vm)
        {
            vmListadoOpciones lis = BuscarOpcionXModuloXSistema(vm);

            return PartialView("_ListadoOpciones", lis);
        }

        [HttpGet]
        public IActionResult AgregarOpcion(int id, int idM)
        {
            vmOpcion model = new vmOpcion(id, idM);
            model.OpcionPadre = BuscarOpcionXModuloXSistema(new vmBuscarOpciones { idsistema = id, idModulo = idM }).Opcions;
            model.OpcionPadre = model.OpcionPadre.Where(t => t.IOpcnPdre == 0).ToList();
            model.UCrcn = HttpContext.User.Identity.Name;
            model.UActlzcn = HttpContext.User.Identity.Name;
            return PartialView("_AgregarEditarOpcion", model);
        }
        [HttpGet]
        public IActionResult EditarOpcion(int id)
        {
            vmOpcion model = OpcionServ.GetOpcion(id);
            model.OpcionPadre = BuscarOpcionXModuloXSistema(new vmBuscarOpciones { idsistema = model.ISstma, idModulo = Convert.ToInt32(model.IMdlo) }).Opcions.Where(x => x.nId != id).ToList();
            model.OpcionPadre = model.OpcionPadre.Where(t => t.IOpcnPdre == 0).ToList();
            return PartialView("_AgregarEditarOpcion", model);
        }
        [HttpGet]
        public IActionResult Opcion(vmOpcion item)
        {
            return PartialView("_AgregarEditarOpcion", null);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AgregarEditarOpcion(vmOpcion vmOpc)
        {
            try
            {
                Opcion opc = null;
                
                if (vmOpc.nId == 0)
                {
                    opc = vmOpc;
                    opc.UActlzcn = HttpContext.User.Identity.Name;
                    opc.UCrcn = HttpContext.User.Identity.Name;
                    OpcionServ.InsertOpcion(opc);
                }
                else
                {
                    opc = OpcionServ.GetOpcion(vmOpc.nId);
                    opc.nId = vmOpc.nId;
                    opc.ISstma = vmOpc.ISstma;
                    opc.IMdlo = vmOpc.IMdlo;
                    opc.Nmbre = vmOpc.Nmbre;
                    opc.IOPdre = vmOpc.IOpcnPdre;
                    opc.URL = vmOpc.URL;
                    opc.Dscrpcn = vmOpc.Dscrpcn;
                    opc.CEstdo = vmOpc.CEstdo;
                    opc.FEstdo = vmOpc.FEstdo;
                    opc.FActlzcn = DateTime.Now;
                    opc.UActlzcn = HttpContext.User.Identity.Name;
                    OpcionServ.UpdateOpcion(opc);
                }
                vmListadoOpciones lis = BuscarOpcionXModuloXSistema(new vmBuscarOpciones { idsistema = Convert.ToInt32(vmOpc.ISstma), idModulo = Convert.ToInt32(vmOpc.IMdlo) });
                return PartialView("_ListadoOpciones", lis);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { Mensaje = ex.Message });
            }
        }
        [HttpGet]
        public IActionResult EliminarOpcion(int id)
        {
            Opcion mod;
            try
            {
                mod = OpcionServ.GetOpcion(id);
                mod.CEstdo = "I";
                OpcionServ.UpdateOpcion(mod);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { Mensaje = ex.Message });
            }

            vmListadoOpciones lis = BuscarOpcionXModuloXSistema(new vmBuscarOpciones { idsistema = Convert.ToInt32(mod.ISstma), idModulo = Convert.ToInt32(mod.IMdlo) });
            return PartialView("_ListadoOpciones", lis);
        }
    }
}