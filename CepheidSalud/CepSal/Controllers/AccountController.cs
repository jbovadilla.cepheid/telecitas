﻿using CepSal.Identity;
using CepSal.ViewModels;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Models;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace CepSal.Controllers
{
    public class AccountController : Controller
    {
        private readonly IAppUserStore _signInManager;
        private readonly IAppRoleStore _roleManager;
        private readonly IServ_Empresa EmpresaServ;
        private readonly IServ_Sucursal SucursalServ;
        public AccountController(IAppUserStore s, IAppRoleStore RoleManager, IServ_Sucursal SucursalServ, IServ_Empresa EmpresaServ)
        {
            this.SucursalServ = SucursalServ;
            this.EmpresaServ = EmpresaServ;
            this._signInManager = s;
            this._roleManager = RoleManager;
        }



        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Login()
        {
#if DEBUG
            ///Se retorna un usuario para evitar entrar al login
            //return Login(new vmLogin { Usuario = "ADM", Contrasena = "Admin@123" }).Result;
#endif
            ViewBag.Empresas = new SucursalController(SucursalServ, EmpresaServ).BuscarEmpresas();
            return View("Index", new vmLogin());
        }

        [HttpPost]
        public async Task<IActionResult> Login(vmLogin vm)
        {
            int _SISTEMA_ = Codigos.CEPHEIDSALUD;
            int _MODULO_ = Codigos.CEPHEIDSALUD;
            try
            {
                //await _signInManager.CreateAsync(new Usuario { CUsro = vm.Usuario, Cntrsna = vm.Contrasena },new CancellationToken());

                var logueando = _signInManager.FindByNameAsync(vm.Usuario, new CancellationToken()).Result;
                if (logueando != null)
                {
                    var contrase = _signInManager.GetPasswordHashAsync(logueando, new CancellationToken()).Result;
                    string idPerfil = await _roleManager.GetRoleIdAsync(logueando, _SISTEMA_, new CancellationToken());
                    if (logueando.Cntrsna.Equals(contrase))
                    {
                        var claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.Name, logueando.CUsro),
                        new Claim("Sistema", _SISTEMA_.ToString()/*falta obtner de algun lado*/),
                        new Claim("Modulo", _MODULO_.ToString()/*falta obtner de algun lado*/),
                        new Claim("Empresa", logueando.IEmprsa.ToString()),
                        new Claim("Sucursal", vm.IdSucursal.ToString()),
                        new Claim(ClaimTypes.Role, idPerfil),
                        new Claim(ClaimTypes.NameIdentifier, logueando.CUsro),
                        new Claim(ClaimTypes.Sid, logueando.nId.ToString())
                    };
                        var userIdentity = new ClaimsIdentity(claims, "login");
                        ClaimsPrincipal principal = new ClaimsPrincipal(userIdentity);
                        HttpContext.User = principal;
                        await HttpContext.SignInAsync(principal);
                        HttpContext.Session.SetString("usuario", logueando.CUsro);

                        //Just redirect to our index after logging in. 
                        return Redirect("/");
                    }
                    else
                    {
                        ViewBag.error = "Usuario o contraseña inválidos.";
                        return View("Index");
                    }
                }
                else
                {
                    ViewBag.error = "Usuario o contraseña inválidos.";
                    return View("Index");
                }
            }
            catch (Exception ex)
            {
                ViewBag.error = ex.Message;
                return View("Index");
            }
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync();
            return Redirect("/");
        }
        [HttpGet]
        public JsonResult BuscarSucursalesXEmpresas(int id)
        {
            var sucursales = new SucursalController(SucursalServ, EmpresaServ).BuscarSucursalxEmpresa(new vmBuscarSucursales { idEmpresa = id });
            return Json(new SelectList(sucursales.Sucursales, "nId", "Nmbre"));
        }
    }
}