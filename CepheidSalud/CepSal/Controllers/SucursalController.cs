﻿using CepSal.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Models;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CepSal.Controllers
{
    [Authorize(Policy = "TienePermisosSucursal")]
    public class SucursalController : Controller
    {
        private readonly IServ_Sucursal SucursalServ;
        private readonly IServ_Empresa EmpresaServ;
        public SucursalController(IServ_Sucursal SucursalServ, IServ_Empresa EmpresaServ)
        {
            this.SucursalServ = SucursalServ;
            this.EmpresaServ = EmpresaServ;
        }
        public List<vmEmpresa> BuscarEmpresas()
        {
            var emp = new List<vmEmpresa>();
            EmpresaServ.GetEmpresas().ToList().ForEach(u =>
            {
                emp.Add(u);
            });
            return emp;
        }
        public vmListadoSucursales BuscarSucursalxEmpresa(vmBuscarSucursales vm)
        {
            vmListadoSucursales lis = new vmListadoSucursales();
            lis.Sucursales = new List<vmSucursal>();
            lis.idEmpresa = vm.idEmpresa;
            SucursalServ.ObtenerSucursalxEmpresa(vm.idEmpresa).ToList().ForEach(u =>
            {
                lis.Sucursales.Add(u);
            });
            return lis;
        }

        [HttpGet]
        public IActionResult Index()
        {
            ViewBag.Empresas = BuscarEmpresas();
            vmBuscarSucursales vm = new vmBuscarSucursales { idEmpresa = 0 };
            return View(vm);
        }
        [HttpPost]
        public IActionResult BuscarSucursalesEmpresa(vmBuscarSucursales vm)
        {
            vmListadoSucursales lis = BuscarSucursalxEmpresa(vm);

            return PartialView("_ListadoSucursales", lis);
        }
        [HttpGet]
        public IActionResult AgregarSucursal(int id)
        {
            vmSucursal model = new vmSucursal(id);
            model.UCrcn = HttpContext.User.Identity.Name;
            model.UActlzcn = HttpContext.User.Identity.Name;
            return PartialView("_AgregarEditarSucursal", model);
        }
        [HttpGet]
        public IActionResult EditarSucursal(int idSuc)
        {
            vmSucursal model = SucursalServ.GetSucursal(idSuc);
            return PartialView("_AgregarEditarSucursal", model);
        }
        [HttpGet]
        public IActionResult Sucursal(vmSucursal item)
        {
           // vmSucursal model = SucursalServ.ObtenerSucursalxEmpresa(idEmp, idSuc);
            return PartialView("_AgregarEditarSucursal", null);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AgregarEditarSucursal(vmSucursal vmSuc)
        {
            try
            {
                Sucursal suc = null;
                
                if (vmSuc.nId == 0)
                {
                   
                    suc = vmSuc;
                    suc.UActlzcn = HttpContext.User.Identity.Name;
                    suc.UCrcn = HttpContext.User.Identity.Name;
                    SucursalServ.InsertSucursal(suc);
                }
                else
                {
                    suc = SucursalServ.GetSucursal(vmSuc.nId);
                    suc.nId = vmSuc.nId;
                    suc.IScrsl = vmSuc.IScrsl;
                    suc.IEmprsa = vmSuc.IEmprsa;
                    suc.Nmbre = vmSuc.Nmbre;
                    suc.Dscrpcn = vmSuc.Dscrpcn;
                    suc.CEstdo = vmSuc.CEstdo;
                    suc.FEstdo = vmSuc.FEstdo;
                    suc.FActlzcn = DateTime.Now;
                    suc.UActlzcn = HttpContext.User.Identity.Name;

                    SucursalServ.UpdateSucursal(suc);
                }
                vmListadoSucursales lis = BuscarSucursalxEmpresa(new vmBuscarSucursales { idEmpresa = Convert.ToInt32(vmSuc.IEmprsa) });
                return PartialView("_ListadoSucursales", lis);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { Mensaje = ex.Message });
            }
        }
        [HttpGet]
        public IActionResult EliminarSucursal(int idSuc)
        {
            Sucursal suc;
            try
            {
                suc = SucursalServ.GetSucursal(idSuc);
                suc.CEstdo = "I";
                SucursalServ.UpdateSucursal(suc);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { Mensaje = ex.Message });
            }

            vmListadoSucursales lis = BuscarSucursalxEmpresa(new vmBuscarSucursales { idEmpresa = Convert.ToInt32(suc.IEmprsa) });
            return PartialView("_ListadoSucursales", lis);
        }
    }
}