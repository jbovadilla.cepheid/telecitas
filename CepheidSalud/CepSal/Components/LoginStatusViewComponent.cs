﻿using CepSal.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using static Models.Codigos;

namespace CepSal.Components
{
    public class LoginStatusViewComponent : ViewComponent
    {
        private readonly IServ_OpcionPerfil OpcionPerServ;
        private readonly IServ_Modulo ModuloServ;

        public LoginStatusViewComponent(IServ_OpcionPerfil OpcionPerServ, IServ_Modulo ModuloServ)
        {
            this.OpcionPerServ = OpcionPerServ;
            this.ModuloServ = ModuloServ;
        }

        public async Task<IViewComponentResult> InvokeAsync(ComponenteLayout view)
        {
            var usrName = HttpContext.User.Identity.Name;// _session.GetString("usuario");
            switch (view)
            {
                case ComponenteLayout.PanelUsuario:
                    return View("LoggedIn", usrName);
                case ComponenteLayout.MenuUsuario:
                    return View("LoggedIn2", usrName);
                case ComponenteLayout.SideBarMenu:
                    return OpcionesSideBarMenu();
                default:
                    return View();
            }
        }
        /// <summary>
        /// obtener modulos para el perfil del usuario
        /// </summary>
        /// <returns></returns>
        public IViewComponentResult OpcionesSideBarMenu()
        {
            var nIdSucursal = HttpContext.User.Claims.ToList().Find(x => x.Type == "Sucursal").Value;
            var IdEmpresa = HttpContext.User.Claims.ToList().Find(x => x.Type == "Empresa").Value;
            var nIdSistema = Convert.ToDecimal(HttpContext.User.Claims.ToList().Find(x => x.Type == "Sistema").Value);
            var nIdModulo = Convert.ToDecimal(HttpContext.User.Claims.ToList().Find(x => x.Type == "Modulo").Value);
            var IdPerfil = Convert.ToDecimal(HttpContext.User.Claims.ToList().Find(x => x.Type == ClaimTypes.Role).Value);
            var nIdUsuario = HttpContext.User.Claims.ToList().Find(x => x.Type == ClaimTypes.Sid).Value;

            vmMenuLateral vm = new vmMenuLateral();
            vm.ListaModulos = new List<ModulosMenu>();

            ModuloServ.ObtenerModuloXSistema(nIdSistema).Where(x => x.CEstdo == "A").ToList().ForEach(u =>
                {
                    var padres = new List<OpcionPadreMenu>();
                    u.Opcion.Where(yy => yy.IOPdre == null && yy.CEstdo == "A" && TienePermisos(nIdSistema, nIdModulo, IdPerfil, yy.IOpcn)).ToList().ForEach(p =>
                           {
                               var hijos = new List<OpcionHijoMenu>();
                               p.OpcionHijo.Where(est => est.CEstdo == "A" && TienePermisos(nIdSistema, nIdModulo, IdPerfil, est.IOpcn)).ToList().ForEach(hh =>
                                   {
                                       hijos.Add(new OpcionHijoMenu
                                       {
                                           Nombre = hh.Nmbre,
                                           Controlador = hh.URL.Split("/")[0],
                                           Accion = hh.URL.Split("/")[1]
                                       });
                                   });
                               padres.Add(new OpcionPadreMenu
                               {
                                   Nombre = p.Nmbre,
                                   Url = p.URL,
                                   Icono = p.Dscrpcn,
                                   ListaOpcionesHijo = hijos,
                               });
                           });
                    vm.ListaModulos.Add(new ModulosMenu
                    {
                        Nombre = u.Nmbre,
                        ListaOpcionesPadres = padres//BuscarOpcionesPadre(nIdSistema, nIdModulo, IdPerfil)
                    });
                });
            return View("MenuSideBar", vm);
        }
        public bool TienePermisos(decimal idsistema, decimal idModulo, decimal idPer, decimal idOpc)
        {
            var nIdSucursal = HttpContext.User.Claims.ToList().Find(x => x.Type == "Sucursal").Value;
            var IdEmpresa = HttpContext.User.Claims.ToList().Find(x => x.Type == "Empresa").Value;
            var nIdSistema = Convert.ToDecimal(HttpContext.User.Claims.ToList().Find(x => x.Type == "Sistema").Value);
            var nIdModulo = Convert.ToDecimal(HttpContext.User.Claims.ToList().Find(x => x.Type == "Modulo").Value);
            var IdPerfil = Convert.ToDecimal(HttpContext.User.Claims.ToList().Find(x => x.Type == ClaimTypes.Role).Value);
            var nIdUsuario = HttpContext.User.Claims.ToList().Find(x => x.Type == ClaimTypes.Sid).Value;

            var opc = OpcionPerServ.GetOpcionPerfils()
                       .FirstOrDefault(x => x.ISstma == idsistema
                       && x.IMdlo == idModulo
                       && x.IPrfl == idPer
                       && x.IOpcn == idOpc);
            return (opc == null) ? false : (opc.CEstdo == "A") ? true : false;
        }
    }
}
