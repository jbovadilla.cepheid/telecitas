﻿/*Metodos ajax*/
success = function (xhr) {
    $('#modal-action').find('.modal-content').html(xhr);
    $('#modal-action').find('.modal-content').change();
};
ExitoGuardar = function (data) {
    $('#modal-action').modal('hide');
    $('#ModalExito').modal('show');
};
/*fin metodos ajax*/

/*validacion modal*/
$('#modal-action').find('.modal-content').change(
    function () {
        $.validator.unobtrusive.parse('#modal-action');
    });

$('#tablaSistema').ToDataTable();
