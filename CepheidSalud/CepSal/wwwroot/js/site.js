﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
//PARA TODAS LAS MODALES
(function ($) {
    function User() {
        var $this = this;

        function initilizeModel() {
            $("#modal-action").on('loaded.bs.modal', function (e) {

            }).on('hidden.bs.modal', function (e) {
                $(this).removeData('bs.modal');
            });
        }
        $this.init = function () {
            initilizeModel();
        }
    }
    $(function () {
        var self = new User();
        self.init();
    })
}(jQuery))
///PARA LA PAGINACION DE LAS TABLAS... 
$.fn.ToDataTable = function () {
    this.dataTable({
        "language": {
            "url": "/lib/bootstrap/dist/js/DataTableSpanish.json"
        }
    });
};

//CARGAR DDL HIJO DEPENDIENDO DEL CAMBIO DEL PADRE
$.fn.OnChangeDdl = function (Hijo, ruta) {
    this.change(function () {
        var url = ruta;
        var ddlSource = this;
        $.getJSON(url, { id: $(ddlSource).val() }, function (data) {
            var items = '';
            $(Hijo).empty();
            items += "<option value='0'> - Seleccione - </option>";
            $.each(data, function (i, obj) {
                items += "<option value='" + obj.value + "'>" + obj.text + "</option>";
            });
            $(Hijo).html(items);
            $(Hijo).change();
        });
    });
}

$.fn.OnChangeDdl1 = function (Nieto,Padre, ruta) {
    this.change(function () {
        var url = ruta;
        var ddlSource = this;
        $.getJSON(url, { id: $(Padre).val(), idMod: $(ddlSource).val() }, function (data) {
            var items = '';
            $(Nieto).empty();
            items += "<option value='0'> - Seleccione - </option>";
            $.each(data, function (i, obj) {
                items += "<option value='" + obj.value + "'>" + obj.text + "</option>";
            });
            $(Nieto).html(items);
            $(Nieto).change();
        });
    });
}


//$("#ddlSistema").change(function () {
//    var url = "Perfil/BuscarModuloXSistema";
//    var ddlSource = "#ddlSistema";
//    $.getJSON(url, { id: $(ddlSource).val() }, function (data) {
//        var items = '';
//        $('#ddlModulo').empty();
//        $.each(data, function (i, objModulo) {
//            items += "<option value='" + objModulo.value + "'>" + objModulo.text + "</option>";

//        });
//        $('#ddlModulo').html(items);
//    });
//});

//$("#ddlSistema").on("change", function () {
//    $list = $("#ddlModulo");
//    $.ajax({
//        url: "Perfil/BuscarModuloXSistema",
//        type: "GET",
//        data: { id: $("#ddlSistema").val() }, //id of the state which is used to extract cities 
//        traditional: true,
//        success: function (result) {
//            $list.empty();
//            $.each(result, function (i, objModulo){
//                $list.append("<option value='" + objModulo.value + "'>" + objModulo.text + "</option>");
//            });
//        },
//        error: function (result) {
//            console.log(result);
//            alert("Something went wrong call the police");
//        }
//    });
//});