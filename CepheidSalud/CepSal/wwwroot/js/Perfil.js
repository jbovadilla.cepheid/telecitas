﻿/*Metodos ajax*/
success = function (xhr) {
    $('#modal-action').find('.modal-content').html(xhr);
    $('#modal-action').find('.modal-content').change();
};
ExitoGuardar = function (data) {
    $('#modal-action').modal('hide');

    if (data.status == 900) {
        $('#ModalError').modal('show');
        $('#tablaPerfil').html(data.ajaxReturn);
        $('#tablaPerfil2').ToDataTable();
    }
    else
        $('#ModalExito').modal('show');
};
success2 = function (data) {
    if (data.status == 900) {
        $('#ModalError').modal('show');
        $('#tablaPerfil').html(data.ajaxReturn);
        $('#tablaPerfil2').ToDataTable();
    }
    else
        $('#tablaPerfil2').ToDataTable();
};
/*fin metodos ajax*/

/*validacion modal*/
$('#modal-action').find('.modal-content').change(
    function () {
        $.validator.unobtrusive.parse('#modal-action');
    });
/*convertir a datatables*/
$('#tablaPerfil2').ToDataTable();

$("#ddlSistema").OnChangeDdl('#ddlModulo', "Perfil/BuscarModuloXSistema");

Abrir = function (data) {
    //muestra
    $("#tablaPerfil").addClass("col-lg-7", "fast");
    $("#detalles").show("slow");
}
function Cerrar() {
    $("#tablaPerfil").removeClass("col-lg-7", "slow");
    $("#detalles").hide("fast");
}