﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;
using Services.Interfaces;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CepSal.Identity
{
    public class TienePermisosHandler : AuthorizationHandler<TienePermisosRequirement>
    {
        //private readonly Repo.AppContext dbContext;
        private readonly IServ_OpcionPerfil OpcionPerServ;
        public TienePermisosHandler(IServ_OpcionPerfil OpcionPerServ)
        {
            this.OpcionPerServ = OpcionPerServ;
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, TienePermisosRequirement requirement)
        {
            if (context.Resource is AuthorizationFilterContext authContext)
            {
                var nIdSistema = Convert.ToDecimal(context.User.Claims.ToList().Find(x => x.Type == "Sistema").Value);
                var nIdModulo = Convert.ToDecimal(context.User.Claims.ToList().Find(x => x.Type == "Modulo").Value);
                var IdPerfil = Convert.ToDecimal(context.User.Claims.ToList().Find(x => x.Type == ClaimTypes.Role).Value);
                if (TienePermisos(nIdSistema, nIdModulo, IdPerfil, requirement.OpcModulo))
                {
                    context.Succeed(requirement);
                }
                else
                {
                    context.Fail();
                }
            }

            return Task.CompletedTask;
        }

        public bool TienePermisos(decimal idsistema, decimal idModulo, decimal idPer, string idOpc)
        {
            var opc = OpcionPerServ.GetOpcionPerfils()
                       .FirstOrDefault(x => x.ISstma == idsistema
                       && x.IMdlo == idModulo
                       && x.IPrfl == idPer
                       && ValidarOpcion(x.Opcion.URL, idOpc));
            return (opc == null) ? false : (opc.CEstdo == "A") ? true : false;
        }
        public bool ValidarOpcion(string url, string opc)
        {
            if (!url.Equals("#"))
            {
                return (url.Split("/")[0].Equals(opc));
            }
            return false;
        }
    }
    public class TienePermisosRequirement : IAuthorizationRequirement
    {
        public string OpcModulo { get; set; }
        public TienePermisosRequirement(string controlador)
        {
            this.OpcModulo = controlador;
        }
    }

}
