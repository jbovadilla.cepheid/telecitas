﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Models.Map
{
    public class Map_Sistema
    {
        
        public Map_Sistema(EntityTypeBuilder<Sistema> entity)
        {
            entity.HasKey(e => e.ISstma);

            entity.ToTable("SEGSIS00", "SEG");

            entity.Property(e => e.ISstma)
                .HasColumnName("ISstma")
                .ValueGeneratedOnAdd();

            entity.Property(e => e.CEstdo)
                .IsRequired()
                .HasColumnName("CEstdo")
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("('A')");

            entity.Property(e => e.Dscrpcn)
                .IsRequired()
                .HasMaxLength(250)
                .IsUnicode(false);

            entity.Property(e => e.FActlzcn)
                .HasColumnName("FActlzcn")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.FCrcn)
                .HasColumnName("FCrcn")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.FEstdo)
                .HasColumnName("FEstdo")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.Nmbre)
                .IsRequired()
                .HasMaxLength(150)
                .IsUnicode(false);

            entity.Property(e => e.UActlzcn)
                .IsRequired()
                .HasColumnName("UActlzcn")
                .HasMaxLength(15)
                .IsUnicode(false);

            entity.Property(e => e.UCrcn)
                .IsRequired()
                .HasColumnName("UCrcn")
                .HasMaxLength(15)
                .IsUnicode(false);
        }
    }
}
