﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Models.Map
{
    public class Map_UsuarioPerfil
    {
       
        public Map_UsuarioPerfil(EntityTypeBuilder<UsuarioPerfil> entity)
        {
            entity.HasKey(e => new { e.IEmprsa, e.ISstma, e.IMdlo, e.IPrfl, e.IPrsna });

            entity.ToTable("SEGSEG04", "SEG");

            entity.HasIndex(e => e.nId)
                 .HasName("UC_SEG04_NID")
                 .IsUnique();

            entity.Property(e => e.nId)
                 .HasColumnName("NID")
                 .ValueGeneratedOnAdd();

            entity.Property(e => e.IEmprsa)
                .HasColumnName("IEmprsa");

            entity.Property(e => e.ISstma)
                .HasColumnName("ISstma");

            entity.Property(e => e.IMdlo)
                .HasColumnName("IMdlo")
                .HasColumnType("numeric(10, 0)");

            entity.Property(e => e.IPrfl)
                .HasColumnName("IPrfl")
                .HasColumnType("numeric(2, 0)");

            entity.Property(e => e.IPrsna)
                .HasColumnName("IPrsna")
                .HasColumnType("numeric(15, 0)");

            entity.Property(e => e.CEstdo)
                .IsRequired()
                .HasColumnName("CEstdo")
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("('A')");

            entity.Property(e => e.FActlzcn)
                .HasColumnName("FActlzcn")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.FCrcn)
                .HasColumnName("FCrcn")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.FEstdo)
                .HasColumnName("FEstdo")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.UActlzcn)
                .IsRequired()
                .HasColumnName("UActlzcn")
                .HasMaxLength(15)
                .IsUnicode(false);

            entity.Property(e => e.UCrcn)
                .IsRequired()
                .HasColumnName("UCrcn")
                .HasMaxLength(15)
                .IsUnicode(false);

            entity.HasOne(d => d.Usuario)
                .WithMany(p => p.UsuarioPerfil)
                .HasForeignKey(d => new { d.IEmprsa, d.IPrsna })
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_SEGSEG0400");

            entity.HasOne(d => d.Perfil)
                .WithMany(p => p.UsuarioPerfil)
                .HasForeignKey(d => new { d.ISstma, d.IMdlo, d.IPrfl })
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_SEGSEG0401");
        }
    }
}
