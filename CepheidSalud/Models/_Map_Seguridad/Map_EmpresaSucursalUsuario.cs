﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Models.Map
{
    public class Map_EmpresaSucursalUsuario
    {
        
        public Map_EmpresaSucursalUsuario(EntityTypeBuilder<EmpresaSucursalUsuario> entity)
        {
            entity.HasKey(e => new { e.IEmprsa, e.IScrsl, e.IPrsna });

            entity.HasIndex(e => e.nId)
                  .HasName("UC_SEG03_NID")
                  .IsUnique();

            entity.ToTable("SEGSEG03", "SEG");

            entity.Property(e => e.nId)
                 .HasColumnName("NID")
                 .ValueGeneratedOnAdd();

            entity.Property(e => e.IEmprsa)
                .HasColumnName("IEmprsa");

            entity.Property(e => e.IScrsl)
                .HasColumnName("IScrsl")
                .HasColumnType("numeric(10, 0)");

            entity.Property(e => e.IPrsna)
                .HasColumnName("IPrsna")
                .HasColumnType("numeric(15, 0)");

            entity.Property(e => e.CEstdo)
                .IsRequired()
                .HasColumnName("CEstdo")
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("('A')");

            entity.Property(e => e.FActlzcn)
                .HasColumnName("FActlzcn")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.FCrcn)
                .HasColumnName("FCrcn")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.FEstdo)
                .HasColumnName("FEstdo")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.UActlzcn)
                .IsRequired()
                .HasColumnName("UActlzcn")
                .HasMaxLength(15)
                .IsUnicode(false);

            entity.Property(e => e.UCrcn)
                .IsRequired()
                .HasColumnName("UCrcn")
                .HasMaxLength(15)
                .IsUnicode(false);

            entity.HasOne(d => d.Usuario)
                .WithMany(p => p.EmpresaSucursalUsuario)
                .HasForeignKey(d => new { d.IEmprsa, d.IPrsna })
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_SEGSEG0301");

            entity.HasOne(d => d.Sucursal)
                .WithMany(p => p.EmpresaSucursalUsuario)
                .HasForeignKey(d => new { d.IEmprsa, d.IScrsl })
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_SEGSEG0300");
        }
    }
}
