﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Models.Map
{
    public class Map_ErrorSeguridad
    {
        
        public Map_ErrorSeguridad(EntityTypeBuilder<ErrorSeguridad> entity)
        {
            entity.HasKey(e => e.IErrr);

            entity.ToTable("SEGERR00", "SEG");

            entity.Property(e => e.IErrr)
                .HasColumnName("IErrr")
                .ValueGeneratedOnAdd();

            entity.Property(e => e.DEMssge)
                .HasColumnName("DEMssge")
                .HasMaxLength(4000);

            entity.Property(e => e.DEPrcdre)
                .HasColumnName("DEPrcdre")
                .HasMaxLength(128);

            entity.Property(e => e.FCrcn)
                .HasColumnName("FCrcn")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.IELne).HasColumnName("IELne");

            entity.Property(e => e.IEmprsa)
                .HasColumnName("IEmprsa");

            entity.Property(e => e.IENmber).HasColumnName("IENmber");

            entity.Property(e => e.IEState).HasColumnName("IEState");

            entity.Property(e => e.IESvrty)
                .HasColumnName("IESvrty")
                .HasMaxLength(4000);

            entity.Property(e => e.UCrcn)
                .IsRequired()
                .HasColumnName("UCrcn")
                .HasMaxLength(15)
                .IsUnicode(false);

            entity.HasOne(d => d.Empresa)
                .WithMany(p => p.ErrorSeguridad)
                .HasForeignKey(d => d.IEmprsa)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_SEGERR00");
        }
    }
}
