﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Models.Map
{
    public class Map_Modulo
    {
        
        public Map_Modulo(EntityTypeBuilder<Modulo> entity)
        {
            entity.HasKey(e => new { e.ISstma, e.IMdlo });

            entity.HasIndex(e => e.nId)
                .HasName("UC_SIS01_NID")
                .IsUnique();

            entity.ToTable("SEGSIS01", "SEG");

            entity.Property(e => e.nId)
                 .HasColumnName("NID")
                 .ValueGeneratedOnAdd();

            entity.Property(e => e.ISstma)
                .HasColumnName("ISstma");

            entity.Property(e => e.IMdlo)
                .HasColumnName("IMdlo")
                .HasColumnType("numeric(10, 0)");

            entity.Property(e => e.CEstdo)
                .IsRequired()
                .HasColumnName("CEstdo")
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("('A')");

            entity.Property(e => e.Dscrpcn)
                .IsRequired()
                .HasMaxLength(250)
                .IsUnicode(false);

            entity.Property(e => e.FActlzcn)
                .HasColumnName("FActlzcn")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.FCrcn)
                .HasColumnName("FCrcn")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.FEstdo)
                .HasColumnName("FEstdo")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.Nmbre)
                .IsRequired()
                .HasMaxLength(150)
                .IsUnicode(false);

            entity.Property(e => e.UActlzcn)
                .IsRequired()
                .HasColumnName("UActlzcn")
                .HasMaxLength(15)
                .IsUnicode(false);

            entity.Property(e => e.UCrcn)
                .IsRequired()
                .HasColumnName("UCrcn")
                .HasMaxLength(15)
                .IsUnicode(false);

            entity.HasOne(d => d.Sistema)
                .WithMany(p => p.Modulo)
                .HasForeignKey(d => d.ISstma)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_SEGSIS01");
        }
    }
}
