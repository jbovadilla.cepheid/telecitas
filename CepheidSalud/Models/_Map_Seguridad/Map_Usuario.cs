﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Models.Map
{
    public class Map_Usuario
    {
      
        public Map_Usuario(EntityTypeBuilder<Usuario> entity)
        {
            entity.HasKey(e => new { e.IEmprsa, e.IPrsna });

            entity.ToTable("SEGSEG02", "SEG");

            entity.Property(e => e.nId)
                 .HasColumnName("NID")
                 .ValueGeneratedOnAdd();

            entity.HasIndex(e => e.CUsro)
                .HasName("UC_SEG02_CUsro")
                .IsUnique();

            entity.Property(e => e.IEmprsa)
                .HasColumnName("IEmprsa");

            entity.Property(e => e.IPrsna)
                .HasColumnName("IPrsna")
                .HasColumnType("numeric(15, 0)");

            entity.Property(e => e.CBlqdo)
                .IsRequired()
                .HasColumnName("CBlqdo")
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("('S')");

            entity.Property(e => e.CCCntrsna)
                .IsRequired()
                .HasColumnName("CCCntrsna")
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("('S')");

            entity.Property(e => e.CEstdo)
                .IsRequired()
                .HasColumnName("CEstdo")
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("('A')");

            entity.Property(e => e.Cntrsna)
                .IsRequired()
                .HasColumnType("varchar(max)")
                .IsUnicode(false);

            entity.Property(e => e.CUsro)
                .IsRequired()
                .HasColumnName("CUsro")
                .HasMaxLength(15)
                .IsUnicode(false);

            entity.Property(e => e.FActlzcn)
                .HasColumnName("FActlzcn")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.FBlqdo)
                .HasColumnName("FBlqdo")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.FCCntrsna)
                .HasColumnName("FCCntrsna")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.FCrcn)
                .HasColumnName("FCrcn")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.FEstdo)
                .HasColumnName("FEstdo")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.TUsro)
                .IsRequired()
                .HasColumnName("TUsro")
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("('I')");

            entity.Property(e => e.UActlzcn)
                .IsRequired()
                .HasColumnName("UActlzcn")
                .HasMaxLength(15)
                .IsUnicode(false);

            entity.Property(e => e.UCrcn)
                .IsRequired()
                .HasColumnName("UCrcn")
                .HasMaxLength(15)
                .IsUnicode(false);

            entity.HasOne(d => d.Empresa)
                .WithMany(p => p.Usuario)
                .HasForeignKey(d => d.IEmprsa)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_SEGSEG0200");
        }
    }
}
