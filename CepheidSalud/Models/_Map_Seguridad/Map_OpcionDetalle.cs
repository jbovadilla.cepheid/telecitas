﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Models.Map
{
    public class Map_OpcionDetalle
    {
        
        public Map_OpcionDetalle(EntityTypeBuilder<OpcionDetalle> entity)
        {
            entity.HasKey(e => new { e.ISstma, e.IMdlo, e.IOpcn, e.IODtlle });

            entity.ToTable("SEGSIS04", "SEG");

            entity.HasIndex(e => e.nId)
                .HasName("UC_SIS04_NID")
                .IsUnique();

            entity.Property(e => e.ISstma).HasColumnName("ISstma");

            entity.Property(e => e.IMdlo)
                .HasColumnName("IMdlo")
                .HasColumnType("numeric(10, 0)");

            entity.Property(e => e.IOpcn)
                .HasColumnName("IOpcn")
                .HasColumnType("numeric(4, 0)");

            entity.Property(e => e.IODtlle)
                .HasColumnName("IODtlle")
                .HasColumnType("numeric(4, 0)");

            entity.Property(e => e.Nmbre)
                .IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false);

            entity.Property(e => e.CEstdo)
                .IsRequired()
                .HasColumnName("CEstdo")
                .HasMaxLength(1)
                .IsUnicode(false)
                .HasDefaultValueSql("('A')");

            entity.Property(e => e.FActlzcn)
                .HasColumnName("FActlzcn")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.FCrcn)
                .HasColumnName("FCrcn")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.FEstdo)
                .HasColumnName("FEstdo")
                .HasColumnType("datetime")
                .HasDefaultValueSql("(getdate())");

            entity.Property(e => e.nId)
                .HasColumnName("NID")
                .ValueGeneratedOnAdd();

            entity.Property(e => e.UActlzcn)
                .IsRequired()
                .HasColumnName("UActlzcn")
                .HasMaxLength(15)
                .IsUnicode(false);

            entity.Property(e => e.UCrcn)
                .IsRequired()
                .HasColumnName("UCrcn")
                .HasMaxLength(15)
                .IsUnicode(false);

            entity.HasOne(d => d.Opcion)
                .WithMany(p => p.OpcionDetalle)
                .HasForeignKey(d => new { d.ISstma, d.IMdlo, d.IOpcn })
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_SEGSIS04");
        }
    }
}
