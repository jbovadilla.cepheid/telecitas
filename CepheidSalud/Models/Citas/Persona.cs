﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
    public class Persona : Auditoria
    {
        public Persona()
        {

        }

        /// <summary>
        /// persona
        /// </summary>
        public int PNOMBRE { get; set; }
        public string SNOMBRE { get; set; }
        public string APATERNO { get; set; }
        public string AMATERNO { get; set; }
        public string TDCMNTO { get; set; }
        public string NDCMNTO { get; set; }
        public string NCELULAR { get; set; }
        public string CRRO { get; set; }
    }
}
