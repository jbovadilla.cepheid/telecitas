﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
    public class Paciente : Auditoria
    {
        public Paciente()
        {
        }

        /// <summary>
        /// Paciente
        /// </summary>
        public int IDSYS00 { get; set; }
        public string IDPRNTSCO { get; set; }
    }
}
