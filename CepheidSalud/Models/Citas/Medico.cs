﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models
{
    public class Medico : Auditoria
    {
        public Medico()
        {
        }

        /// <summary>
        /// medico
        /// </summary>
        public int IDSYS00 { get; set; }
        public string CMP { get; set; }
    }
}
