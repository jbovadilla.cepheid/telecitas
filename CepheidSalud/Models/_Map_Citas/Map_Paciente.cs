﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Map
{
    public class Map_Paciente
    {
        public Map_Paciente(EntityTypeBuilder<Paciente> entity)
        {
            entity.HasKey(e => new { e.nId });

            entity.HasIndex(e => e.IDSYS00)
                  .HasName("UQ_GCSYS01_00")
                  .IsUnique();

            entity.ToTable("GCSYS02", "CIT");

            entity.Property(e => e.nId)
                 .HasColumnName("NID")
                 .ValueGeneratedOnAdd();

            entity.Property(e => e.IDPRNTSCO)
                .HasColumnName("IDPRNTSCO")
                .IsRequired()
                .HasMaxLength(1)
                .IsUnicode(false);
        }
    }
}
