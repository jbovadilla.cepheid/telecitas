﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Map
{
    public class Map_Persona
    {
        public Map_Persona(EntityTypeBuilder<Persona> entity)
        {
            entity.HasKey(e => new { e.nId });

            entity.HasIndex(e => new { e.TDCMNTO, e.NDCMNTO })
                  .HasName("UQ_GCSYS00_00")
                  .IsUnique();

            entity.ToTable("GCSYS00", "CIT");

            entity.Property(e => e.nId)
                 .HasColumnName("NID")
                 .ValueGeneratedOnAdd();

            entity.Property(e => e.PNOMBRE)
                .HasColumnName("PNOMBRE")
                .IsRequired()
                .HasMaxLength(25)
                .IsUnicode(false);

            entity.Property(e => e.SNOMBRE)
                .HasColumnName("SNOMBRE")
                .HasMaxLength(25)
                .IsUnicode(false);

            entity.Property(e => e.APATERNO)
                .HasColumnName("APATERNO")
                //.IsRequired()
                .HasMaxLength(30)
                .IsUnicode(false);

            entity.Property(e => e.AMATERNO)
                //.IsRequired()
                .HasColumnName("AMATERNO")
                .HasMaxLength(25)
                .IsUnicode(false);

            entity.Property(e => e.TDCMNTO)
                .IsRequired()
                .HasColumnName("TDCMNTO")
                .HasMaxLength(1)
                .IsUnicode(false);

            entity.Property(e => e.NDCMNTO)
                .IsRequired()
                .HasColumnName("NDCMNTO")
                .HasMaxLength(15)
                .IsUnicode(false);

            entity.Property(e => e.NCELULAR)
                //.IsRequired()
                .HasColumnName("NCELULAR")
                .HasMaxLength(9)
                .IsUnicode(false);

            entity.Property(e => e.CRRO)
                //.IsRequired()
                .HasColumnName("CRRO")
                .HasMaxLength(100)
                .IsUnicode(false);

            
        }
    }
}
