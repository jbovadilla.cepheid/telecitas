﻿namespace Models
{
    public class Resultado
    {
        public static bool Exito;
        public static int Codigo;
        public static string Mensaje;
        
        public Resultado(bool exito, int codigo, string mensaje)
        {
            Exito = exito;
            Codigo = codigo;
            Mensaje = mensaje;
        }
        public Resultado(bool exito, int codigo)
        {
            Exito = exito;
            Codigo = codigo;
        }
    }
}
