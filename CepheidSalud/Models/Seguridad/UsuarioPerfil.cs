﻿namespace Models
{
    public class UsuarioPerfil : Auditoria
    {
        public UsuarioPerfil()
        {

        }
        
        /// <summary>
        /// empresa
        /// </summary>
        public int IEmprsa { get; set; }
        /// <summary>
        /// sucursal
        /// </summary>
        public int ISstma { get; set; }
        /// <summary>
        /// modulo
        /// </summary>
        public decimal IMdlo { get; set; }
        /// <summary>
        /// Perfil
        /// </summary>
        public decimal IPrfl { get; set; }
        /// <summary>
        /// persona
        /// </summary>
        public decimal IPrsna { get; set; }


        public virtual Usuario Usuario { get; set; }
        public virtual Perfil Perfil { get; set; }
    }
}
