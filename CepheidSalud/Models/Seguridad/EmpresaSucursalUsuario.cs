﻿namespace Models
{
    public class EmpresaSucursalUsuario : Auditoria
    {
        public EmpresaSucursalUsuario()
        {

        }
        
        /// <summary>
        /// empresa
        /// </summary>
        public int IEmprsa { get; set; }
        /// <summary>
        /// sucursal
        /// </summary>
        public decimal IScrsl { get; set; }
        /// <summary>
        /// persona
        /// </summary>
        public decimal IPrsna { get; set; }

        public virtual Sucursal Sucursal { get; set; }
        public virtual Usuario Usuario { get; set; }
    }
}
