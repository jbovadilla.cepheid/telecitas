﻿namespace Models
{
    public class OpcionPerfilDetalle : Auditoria
    {
        public OpcionPerfilDetalle()
        {

        }
       
        /// <summary>
        /// sistema
        /// </summary>
        public int ISstma { get; set; }
        /// <summary>
        /// modulo
        /// </summary>
        public decimal IMdlo { get; set; }
        /// <summary>
        /// perfil
        /// </summary>
        public decimal IPrfl { get; set; }
        /// <summary>
        /// opcion
        /// </summary>
        public decimal IOpcn { get; set; }
        /// <summary>
        /// opcion detalle
        /// </summary>
        public decimal IODtlle { get; set; }
        /// <summary>
        /// habilitado
        /// </summary>
        public string CHbltdo { get; set; }
        /// <summary>
        /// estructura
        /// </summary>
        public string CEscrtra { get; set; }
        /// <summary>
        /// modificacion
        /// </summary>
        public string CMdfccn { get; set; }


        public virtual OpcionPerfil OpcionPerfil { get; set; }
        public virtual OpcionDetalle OpcionDetalle { get; set; }
    }
}
