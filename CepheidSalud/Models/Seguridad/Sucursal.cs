﻿using System.Collections.Generic;

namespace Models
{
    public class Sucursal : Auditoria
    {
       
        public Sucursal()
        {
           
            this.EmpresaSucursalUsuario = new HashSet<EmpresaSucursalUsuario>();
        }
        /// <summary>
        /// empresa
        /// </summary>
        public int IEmprsa { get; set; }
        /// <summary>
        /// sucursal
        /// </summary>
        public decimal IScrsl { get; set; }
        /// <summary>
        /// nombre
        /// </summary>
        public string Nmbre { get; set; }
        /// <summary>
        /// descripcion
        /// </summary>
        public string Dscrpcn { get; set; }


       
        public virtual Empresa Empresa { get; set; }
        public virtual ICollection<EmpresaSucursalUsuario> EmpresaSucursalUsuario { get; set; }
    }

}
