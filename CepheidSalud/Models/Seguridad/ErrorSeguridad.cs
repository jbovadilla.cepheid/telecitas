﻿using System;

namespace Models
{
    public class ErrorSeguridad : AuditoriaErr
    {
        public ErrorSeguridad()
        {

        }
      
        /// <summary>
        /// error
        /// </summary>
        public int IErrr { get; set; }
        /// <summary>
        /// empresa
        /// </summary>
        public int IEmprsa { get; set; }
        /// <summary>
        /// estado empresa nombre ???? 
        /// </summary>
        public Nullable<int> IENmber { get; set; }
        /// <summary>
        /// severidad
        /// </summary>
        public string IESvrty { get; set; }
        /// <summary>
        /// estado
        /// </summary>
        public Nullable<int> IEState { get; set; }
        /// <summary>
        /// descripcion procedure
        /// </summary>
        public string DEPrcdre { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Nullable<int> IELne { get; set; }
        /// <summary>
        /// mensaje error 
        /// </summary>
        public string DEMssge { get; set; }

        public virtual Empresa Empresa { get; set; }
    }
}
