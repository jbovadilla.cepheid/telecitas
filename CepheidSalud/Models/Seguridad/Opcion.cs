﻿using System;
using System.Collections.Generic;

namespace Models
{
    public class Opcion : Auditoria
    {
        
        public Opcion()
        {
            this.OpcionDetalle = new HashSet<OpcionDetalle>();
            this.OpcionPerfil = new HashSet<OpcionPerfil>();
            this.OpcionHijo = new HashSet<Opcion>();
        }
        /// <summary>
        /// sistema
        /// </summary>
        public int ISstma { get; set; }
        /// <summary>
        /// modulo
        /// </summary>
        public decimal IMdlo { get; set; }
        /// <summary>
        /// opcion
        /// </summary>
        public decimal IOpcn { get; set; }
        /// <summary>
        /// opcion padre
        /// </summary>
        public Nullable<decimal> IOPdre { get; set; }
        /// <summary>
        /// nombre
        /// </summary>
        public string Nmbre { get; set; }
        /// <summary>
        /// descripcion
        /// </summary>
        public string Dscrpcn { get; set; }
        /// <summary>
        /// url
        /// </summary>
        public string URL { get; set; }


        public virtual Modulo Modulo { get; set; }
        public virtual Opcion OpcionPadre { get; set; }
        public virtual ICollection<Opcion> OpcionHijo { get; set; }
        public virtual ICollection<OpcionDetalle> OpcionDetalle { get; set; }
        public virtual ICollection<OpcionPerfil> OpcionPerfil { get; set; }
    }
}
