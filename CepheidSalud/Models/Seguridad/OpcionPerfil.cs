﻿using System.Collections.Generic;

namespace Models
{
    public class OpcionPerfil : Auditoria
    {
        
        public OpcionPerfil()
        {
            this.OpcionPerfilDetalle = new HashSet<OpcionPerfilDetalle>();
        }
        /// <summary>
        /// sistema
        /// </summary>
        public int ISstma { get; set; }
        /// <summary>
        /// modulo
        /// </summary>
        public decimal IMdlo { get; set; }
        /// <summary>
        /// perfil
        /// </summary>
        public decimal IPrfl { get; set; }
        /// <summary>
        /// opcion
        /// </summary>
        public decimal IOpcn { get; set; }
        /// <summary>
        /// habilitado
        /// </summary>
        public string CHbltdo { get; set; }
        /// <summary>
        /// estructura
        /// </summary>
        public string CEscrtra { get; set; }
        /// <summary>
        /// modificacion
        /// </summary>
        public string CMdfccn { get; set; }

        public virtual Perfil Perfil { get; set; }
        public virtual Opcion Opcion { get; set; }
        public virtual ICollection<OpcionPerfilDetalle> OpcionPerfilDetalle { get; set; }
    }
}
