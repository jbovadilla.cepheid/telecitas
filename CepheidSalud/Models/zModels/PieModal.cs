﻿namespace Models
{
    public class PieModal
    {
        public string SubmitButtonText { get; set; } = "Guardar";
        public string CancelButtonText { get; set; } = "Cancelar";
        public string AceptButtonText { get; set; } = "Aceptar";
        public string SubmitButtonID { get; set; } = "btn-guardar";
        public string CancelButtonID { get; set; } = "btn-cancelar";
        public string AceptButtonID { get; set; } = "btn-Aceptar";
        public bool OnlyAceptButton { get; set; }
    }
    public class CabeceraModal
    {
        public string Heading { get; set; }
        public Codigos.ColorModal Color { get; set; }
        public string TipoModal
        {
            get
            {
                switch (this.Color)
                {
                    case Codigos.ColorModal.Error:
                        return "bg-danger";
                    case Codigos.ColorModal.Alerta:
                        return "bg-warning";
                    case Codigos.ColorModal.Exito:
                        return "bg-success";
                    default:
                        return "";
                }
            }
        }
    }
}
