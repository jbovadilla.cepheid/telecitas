﻿namespace Models
{
    public class ModalModel
    {
        public CabeceraModal CabeceraModal { get; set; }
        public PieModal PieModal { get; set; }
        public string ID { get; set; }
        public string AreaLabeledId { get; set; }
        public Codigos.TamanoModal Size { get; set; }
        public Codigos.ColorModal Color { get; set; }
        public string Message { get; set; }
        public string ModalSizeClass
        {
            get
            {
                switch (this.Size)
                {
                    case Codigos.TamanoModal.Pequena:
                        return "modal-sm";
                    case Codigos.TamanoModal.Grande:
                        return "modal-lg";
                    case Codigos.TamanoModal.Mediana:
                    default:
                        return "";
                }
            }
        }

    }
}
