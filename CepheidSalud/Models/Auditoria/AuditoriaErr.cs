﻿using System;

namespace Models
{
    public class AuditoriaErr
    {
        
        /// <summary>
        /// identificador tabla
        /// </summary>
        public int nId { get; set; }
        /// <summary>
        /// Fecha de Creacion
        /// </summary>
        public DateTime FCrcn { get; set; }
        /// <summary>
        /// Usuario creador
        /// </summary>
        public string UCrcn { get; set; }
    }

    public class Auditoria : AuditoriaErr
    {
        
        /// <summary>
        /// Cambio Estado
        /// </summary>
        public string CEstdo { get; set; }
        /// <summary>
        /// Fecha cambio Estado
        /// </summary>
        public DateTime FEstdo { get; set; }
        /// <summary>
        /// Fecha actualizacion
        /// </summary>
        public DateTime FActlzcn { get; set; }
        /// <summary>
        /// Usuario actualizacion
        /// </summary>
        public string UActlzcn { get; set; }
    }
}
